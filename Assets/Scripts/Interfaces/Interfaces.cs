﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface IguiPanel
{
    void Show();
    void Hide();
    void Initialization();
}

public interface IgameHUD: IguiPanel
{
    void StartEnergyInitialization(int startEnergy);
}