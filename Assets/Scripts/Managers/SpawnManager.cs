﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//spawner, interactive prefabs pooling controller
public class SpawnManager : MonoBehaviour {

    public static SpawnManager Instance
    {
        get
        {
            if (s_Instance != null)
                return s_Instance;

            s_Instance = FindObjectOfType<SpawnManager>();

            if (s_Instance != null)
                return s_Instance;

            return s_Instance;
        }
    }

    protected static SpawnManager s_Instance;

    private Vector3[] playerSpawnPos;
    public GameObject[] playerPrefabs;

    private GameObject[] activePlayerObj;
    public GameObject[] activePlayerGet { get { return activePlayerObj; } }

    //spawned prefabs cache
    List<GameObject> spawnedObjs = new List<GameObject>(); //interactive objects spawn list
    List<GameObject> postHits = new List<GameObject>(); // plaers hit positions markers
    List<GameObject> playerEnergyObjs = new List<GameObject>();//player energy ui elements
    private Transform playerEnergyRoot;

    //prefabs
    public GameObject hitPrefab;
    public GameObject postHitPrefab;
    public GameObject playerEnergyPrefab;
    public GameObject playerDeadPrefab;
    public GameObject trajectoryDotPrefab;


    public void SpawnManagerSetup(Vector3[] _playerSpawnPos)
    {
        playerSpawnPos = _playerSpawnPos;
    }

    public void PlayerDeadFx(int playerIndex)
    {
        TrashMan.spawn(playerDeadPrefab, activePlayerObj[playerIndex].transform.position, Quaternion.identity);
    }


    public void PlayerEnergySetup(Transform tr, int energy)
    {
        CleanPlayerEnergy();

        playerEnergyRoot = tr;

        for (int i = 0; i < energy; i++)
        {
            SpawnPlayerEnergyUi();
        }
    }

    public void LevelFailedDespawnActivePlayers()
    {

        for (int i = 0; i < activePlayerObj.Length; i++)
        {
            activePlayerObj[i].GetComponent<PlayerController>().PlayerPause();
            PlayerDeadFx(i);
        }

    }
    public void SpawnPlayerEnergyUi()
    {
        GameObject obj = TrashMan.spawn(playerEnergyPrefab, Vector3.zero, Quaternion.identity);

        obj.transform.parent = playerEnergyRoot;
        obj.transform.localPosition = Vector3.zero;
        obj.transform.localScale = Vector3.one;

        playerEnergyObjs.Add(obj);
    }

    // player energy ui element update
    public void PlayerEnergyUpdate(bool levelEnergy)
    {
        if(levelEnergy)
        {
            SpawnPlayerEnergyUi();
        }
        else
        {
            int despawnIndex = playerEnergyObjs.Count - 1;

            if (despawnIndex >= 0)
            {
                TrashMan.despawn(playerEnergyObjs[despawnIndex]);
                playerEnergyObjs.Remove(playerEnergyObjs[despawnIndex]);
            }
        }

    }

    public void CleanPlayerEnergy()
    {
        if (playerEnergyObjs.Count > 0)
        {
            for (int i = 0; i < playerEnergyObjs.Count; i++)
            {
                TrashMan.despawn(playerEnergyObjs[i]);
            }
            playerEnergyObjs.Clear();
        }
    }

   // spawn player hits position marker
    public void PostHitSpawn(Vector3 pos)
    {
        TrashMan.spawn(hitPrefab, pos, Quaternion.identity);
        GameObject postFx = TrashMan.spawn(postHitPrefab, pos, Quaternion.identity);
        postHits.Add(postFx);
    }

    public void PostHitsClean()
    {
        for (int i = 0; i < postHits.Count; i++)
        {
            TrashMan.despawn(postHits[i]);
        }
    }

    public void PlayerSpawn(int playerIndex)
    {
        if (activePlayerObj != null && activePlayerObj.Length> 0)
        {
            DespawnPlayer();
        }
        activePlayerObj = new GameObject[playerSpawnPos.Length];

        for (int i = 0; i < activePlayerObj.Length; i++)
        {
            activePlayerObj[i] = TrashMan.spawn(playerPrefabs[playerIndex], playerSpawnPos[i], Quaternion.identity);
            activePlayerObj[i].name = "player" + i;
            activePlayerObj[i].GetComponent<PlayerController>().PlayerSetup(i);      
        }
        activePlayerObj[0].GetComponent<PlayerController>().SelectPlayer();

    }

    public void DespawnPlayer()
    {
        for (int i = 0; i < activePlayerObj.Length; i++)
        {
            TrashMan.despawn(activePlayerObj[i]);
        }
    }
}
