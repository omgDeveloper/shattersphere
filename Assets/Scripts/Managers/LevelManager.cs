﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//scenes menagment, load, unload, reload levels/scenes

public class LevelManager : MonoBehaviour {

    public GameObject trashManPrefab;
    private Transform interactiveObjs;

    public LevelConfigData levelConfig;
    public LevelConfigData LevelConfigGet { get { return levelConfig; } }

    private int actualLevelSceneIndex;
    public int ActualLevelIndexGet { get { return actualLevelSceneIndex; } }
    private int loadSceneMode;
    private Scene loadedLevelScene;
    private AsyncOperation ao;
    private string LoadLevelSceneName;

    void Start()
    {
        LevelInitialization();
    }

    public void LevelInitialization()
    {
        interactiveObjs = GameObject.Find("Interactive Objs").transform;
        GameManager gameMgr = GameObject.Find("GameManager").GetComponent<GameManager>();

        gameMgr.GameManagerSetup(this.gameObject);

        levelConfig.interactiveObjects = interactiveObjs.GetComponentsInChildren<ObjectBase>();

        GameObject trashGlobal = Instantiate(trashManPrefab, Vector3.zero, Quaternion.identity) as GameObject;
        trashGlobal.name = "TrashManGlobal";

        levelConfig.gameCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        levelConfig.gameCamera.transform.position = new Vector3(levelConfig.gameCamera.transform.position.x, levelConfig.gameCamera.transform.position.y, levelConfig.gameCamera.transform.position.z);

        gameMgr.GameSetup(trashGlobal);
        levelConfig.relasedPlayers = new bool[levelConfig.playerSpawnPosition.Length];
        for (int i = 0; i < levelConfig.relasedPlayers.Length; i++)
        {
            levelConfig.relasedPlayers[i] = false;
        }

        Prime31.ZestKit.ZestKit.removeAllTweensOnLevelLoad = true;
    }

    //Reset level without unloading
    public void Reset()
    {
        for (int i = 0; i < levelConfig.interactiveObjects.Length; i++)
        {
            levelConfig.interactiveObjects[i].Reset();
        }

        for (int i = 0; i < levelConfig.relasedPlayers.Length; i++)
        {
            levelConfig.relasedPlayers[i] = false;
        }

    }

    //level setup trigger
    public void Setup()
    {
        for (int i = 0; i < levelConfig.interactiveObjects.Length; i++)
        {
            levelConfig.interactiveObjects[i].Setup();
        }
    }

}
