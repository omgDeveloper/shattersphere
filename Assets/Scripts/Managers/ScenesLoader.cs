﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesLoader : MonoBehaviour
{

    public static ScenesLoader Instance
    {
        get
        {
            if (s_Instance != null)
                return s_Instance;

            s_Instance = FindObjectOfType<ScenesLoader>();

            if (s_Instance != null)
                return s_Instance;

            Create();

            return s_Instance;
        }
    }
    public static void Create()
    {
        ScenesLoader controllerPrefab = Resources.Load<ScenesLoader>("ScenesLoader");
        s_Instance = Instantiate(controllerPrefab);
    }
    protected static ScenesLoader s_Instance;

   [SerializeField] private string mainMenuSceneName = "UltraGameInit";
    [SerializeField] private string levelsScenesPrefix = "Level";

    private int actualLevelSceneIndex;
    public int ActualLevelIndexGet { get { return actualLevelSceneIndex; } }
    private int loadSceneMode;
    private Scene loadedLevelScene;
    private AsyncOperation ao;
    private string LoadLevelSceneName;

    void Awake()
    {
        this.transform.parent = null;

        if (Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
    }

    public void LoadNextLevel()
    {
        int stars = GameManager.Instance.PlayerActualStarsGetSet;

        StartCoroutine(LoadNextCorutine());
    }

    IEnumerator LoadNextCorutine()
    {
        int level = actualLevelSceneIndex;
        int alllevels = UIManager.Instance.levelsInEpisodesGet * UIManager.Instance.allEpisodesLenghtGet;

        if (level < alllevels - 1) //back to main menu after last level
        {
            level++;
        }
        else
        {
            level = 0;
        }

        actualLevelSceneIndex = level;

        UIManager.Instance.LoadingScreenShow(true);
        yield return new WaitForEndOfFrame();
        UnloadActiveLevel();
        yield return new WaitForEndOfFrame();

        

        StartCoroutine(AsynchronousSceneLoad());
    }


    public void LoadMainMenu()
    {
        LoadLevelScene(0);
    }

    //reload, reset and reloading
    public void ReloadScene()
    {
        StartCoroutine(ReloadCorutine());
    }

    IEnumerator ReloadCorutine()
    {

        yield return new WaitForEndOfFrame();
        UnloadActiveLevel();
        yield return new WaitForEndOfFrame();

        StartCoroutine(AsynchronousSceneLoad());
    }

    //load scene from scene loader ui
    public void LoadLevelScene(int sceneIndex)
    {
        actualLevelSceneIndex = sceneIndex;
        loadSceneMode = 1;

        StartCoroutine(AsynchronousSceneLoad());
    }

    //unload active scene and elements
    public void UnloadActiveLevel()
    {
        SpawnManager.Instance.CleanPlayerEnergy();

        if (loadedLevelScene.IsValid())
        {
            GameObject[] levelRoots = loadedLevelScene.GetRootGameObjects();
            SceneManager.UnloadSceneAsync(loadedLevelScene.name);

            GameObject zesKit = GameObject.Find("ZestKit");
            DestroyImmediate(zesKit); //remove tweener instance

            SceneManager.UnloadSceneAsync(loadedLevelScene);
        }

    }

    IEnumerator AsynchronousSceneLoad()
    {
        string sceneToLoadName = mainMenuSceneName;

        if (actualLevelSceneIndex >= 1)
        {
            sceneToLoadName = levelsScenesPrefix + actualLevelSceneIndex.ToString("00");
        }
 
        UIManager.Instance.LoadingScreenShow(true);

        yield return new WaitForSeconds(1f);

        ao = SceneManager.LoadSceneAsync(sceneToLoadName);
        ao.allowSceneActivation = false;

        bool load = true;
        while (!ao.isDone)
        {

            float progress = Mathf.Clamp01(ao.progress / 0.9f);


            if (ao.progress >= 0.9f && load)
            {
                load = false;

                UIManager.Instance.LoadingScreenShow(false);
                yield return new WaitForEndOfFrame();
                loadedLevelScene = SceneManager.GetSceneByName(sceneToLoadName);
                yield return new WaitForEndOfFrame();
          
            }

            yield return null;
        }

    }

    public void ActivateScene()
    {
        ao.allowSceneActivation = true;
    }
}
