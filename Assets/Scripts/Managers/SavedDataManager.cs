﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;


//save, load user data

public class SavedDataManager : MonoBehaviour {

    public static SavedDataManager Instance
    {
        get
        {
            if (s_Instance != null)
                return s_Instance;

            s_Instance = FindObjectOfType<SavedDataManager>();

            if (s_Instance != null)
                return s_Instance;

            Create();

            return s_Instance;
        }
    }
    public static void Create()
    {
        SavedDataManager controllerPrefab = Resources.Load<SavedDataManager>("SaveManager");
        s_Instance = Instantiate(controllerPrefab);
    }

    protected static SavedDataManager s_Instance;

    private GameProgress loadedProgres;

    private string saveDataPath;
    private string gameSaveName = "playerProgress.dat";

    private GameSettings gameSettings;
    private string gameSettingsFilename = "settings.dat"; 

    public string LocalDataPath { get { return saveDataPath; } }
    public GameProgress GameProgresGetSet { get { return loadedProgres; } set { loadedProgres = value; } }
    public GameSettings GameSettingsGetSet { get { return gameSettings; } set { gameSettings = value; } }

    void Awake()
    {
        this.transform.parent = null;

        if (Instance != this)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);

        saveDataPath = Application.persistentDataPath;
        LoadGameSettings();
        LoadGameSavedData();
    }



    public void LoadGameSettings()
    {
        string gameSettingsFile = Path.Combine(saveDataPath, gameSettingsFilename);
        gameSettings = new GameSettings();

        if (File.Exists(gameSettingsFile))
        {
            gameSettings = JsonUtility.FromJson<GameSettings>(LoadJsonDataFromFile(gameSettingsFile));
        }
        else
        {
            gameSettings.autoLoad = false;
            gameSettings.soundFx = true;
            gameSettings.musicFx = true;
            gameSettings.lastlevel = 0;
        }

        gameSettings.autoLoad = true;
    }

    public void SaveGameSettingsData()
    {
        string gameSettingsFilePath = Path.Combine(saveDataPath, gameSettingsFilename);

        string jsonData = JsonUtility.ToJson(gameSettings);

        SaveToFile(gameSettingsFilePath, jsonData);
    }

    public void LoadGameSavedData()
    {
        string gameSaveFilePath = Path.Combine(saveDataPath, gameSaveName);
        loadedProgres = new GameProgress();

        if (File.Exists(gameSaveFilePath))
        {
            loadedProgres = JsonUtility.FromJson<GameProgress>(LoadJsonDataFromFile(gameSaveFilePath));
        }
        else
        {
            //default player progress setting
            int levels = 200;
            loadedProgres.isLevelUnlocked = new bool[levels];
            loadedProgres.stars = new int[levels];

            for (int i = 0; i < levels; i++)
            {
                loadedProgres.isLevelUnlocked[i] = false;
                loadedProgres.stars[i] = 0;
            }

            loadedProgres.isLevelUnlocked[0] = true;
            loadedProgres.stars[0] = 0;
            loadedProgres.isLevelUnlocked[1] = true;
            loadedProgres.stars[1] = 0;

        }
    }

    IEnumerator SaveProgress()
    {
        yield return new WaitForEndOfFrame();
        SavePlayerProgressData();
    }

    public void UnlockLevelScene(int levelSceneIndex, int stars)
    {
        int actualStars = stars;
        int savedStars = loadedProgres.stars[levelSceneIndex];

        if (actualStars > savedStars)
            loadedProgres.stars[levelSceneIndex] = actualStars;

        loadedProgres.isLevelUnlocked[levelSceneIndex+1] = true;

        SavePlayerProgressData();
    }

     public void SavePlayerProgressData()
      {
        string gameSettingsFilePath = Path.Combine(saveDataPath, gameSaveName);

        string jsonData = JsonUtility.ToJson(loadedProgres);

        SaveToFile(gameSettingsFilePath, jsonData);
    }

    private void SaveToFile(string filePath, string jsonData)
    {
        StreamWriter writer = new StreamWriter(filePath);
        writer.Write(jsonData);
        writer.Close();
    }

    private string LoadJsonDataFromFile(string filePath)
    {
        StreamReader reader = new StreamReader(filePath);
        string jsonData = reader.ReadToEnd();
        reader.Close();

        return jsonData;
    }
}