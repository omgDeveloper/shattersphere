﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//game states and control

public class GameManager : MonoBehaviour {

    public static GameManager Instance
    {
        get
        {
            if (s_Instance != null)
                return s_Instance;

            s_Instance = FindObjectOfType<GameManager>();

            if (s_Instance != null)
                return s_Instance;

            Create();

            return s_Instance;
        }
    }

    public static void Create()
    {
        GameManager controllerPrefab = Resources.Load<GameManager>("GameManager");
        s_Instance = Instantiate(controllerPrefab);
    }

    protected static GameManager s_Instance;

    private LevelManager levelScr;
    CameraShake cameraShake;

    private int playerDefaultEnergy = 3;

    public enum gameState
    {
        mainMenu,
        levelSelect,
        loading,
        gamePlay,
        pause,
        levelCompleted,
        levelFailed,
        whitePanel
    }
    public gameState state;
    public gameState gameStateGetSet { get { return state; } set { state = value; } }

    private int playerEnergy;
    public int playerEnergyGet { get { return playerEnergy; } }
    private int levelStars;
    public int levelStarsGetSet { get { return levelStars; } set { levelStars = value; } }

    public int playerStars;
    public int PlayerActualStarsGetSet { get { return playerStars; } set { playerStars = value; } }

    private bool isReloading;
    public bool IsReloadingGet { get { return isReloading; } }
    private float timeDelay = 0.25f;

    private int levelsGoals;
    private bool allPlayerRelased;
    public bool AllPlayerRelasedGet { get { return allPlayerRelased; } }

    public static event Action OnAdvertisement;
    public static event Action OnRateMe;
    public static event Action OnLeaderboard;
    public static event Action<int> OnPostScores;
    public static event Action OnUnlockArchivment;


    void Awake()
    {
        this.transform.parent = null;

        if (Instance != this)
            {
                Destroy(gameObject);
                return;
            }

            DontDestroyOnLoad(gameObject);
    }

    public bool IsSelected(GameObject obj)
    {
        bool isSelected = false;

        if (InputManager.Instance.SelectedPlayerGetSet != null && obj == InputManager.Instance.SelectedPlayerGetSet.gameObject)
            isSelected = true;

        return isSelected;
    }

    public Camera GameCamera()
    {
        return  levelScr.LevelConfigGet.gameCamera;
    }

    public Transform GetPlayerPosition(int index)
    {
        return SpawnManager.Instance.activePlayerGet[index].transform;
    }

    public void CameraShake(float power)
    {
        cameraShake.ShakeCamera(power, 0.015f);
    }

    public void PlayerReleasedSet(int index)
    {
        levelScr.LevelConfigGet.relasedPlayers[index] = true;
        GamePlayersCheck();
    }

    //check if all players on level are relased
    public void GamePlayersCheck()
    {
        if (!allPlayerRelased)
        {
            int lenght = levelScr.LevelConfigGet.playerSpawnPosition.Length;

            int players = 0;

            for (int i = 0; i < lenght; i++)
            {
                if (levelScr.LevelConfigGet.relasedPlayers[i])
                    players++;
            }

            if (players == lenght)
                allPlayerRelased = true;
        }
    }

    public void LevelComplete()
    {
        levelsGoals--;

        if (levelsGoals == 0)
        {
            isReloading = true;

            int actualLevelSceneIndex = ScenesLoader.Instance.ActualLevelIndexGet;
            SavedDataManager.Instance.UnlockLevelScene(actualLevelSceneIndex, playerStars);
            StartCoroutine(LevelCompleteDelay());
            
        }
    }

    IEnumerator LevelCompleteDelay()
    {
        state = gameState.levelCompleted;
        yield return new WaitForSeconds(timeDelay);

        if (SavedDataManager.Instance.GameSettingsGetSet.autoLoad)
            ScenesLoader.Instance.LoadNextLevel();
        else
            UIManager.Instance.LevelCompletedShow(true, playerStars);
    }

    public void LevelFailed()
    {
        if (state != gameState.levelFailed && !isReloading)
            StartCoroutine(LevelFailedDelay());
    }

    public void OutOfBoundaries()
    {
        if (state != gameState.levelFailed)
            Invoke("LevelFailed", 1f);
    }

    IEnumerator LevelFailedDelay()
    {
        isReloading = true;
        state = gameState.levelFailed;

            SpawnManager.Instance.LevelFailedDespawnActivePlayers();
        yield return new WaitForSeconds(timeDelay);
            UIManager.Instance.WhitePanelShow(true);
            yield return new WaitForSeconds(timeDelay);
            LevelSetup();
    }

    public void LoadMainMenu()
    {
        ScenesLoader.Instance.LoadMainMenu();
    }

    public void LevelReload()
    {
        if(OnAdvertisement !=null)
            OnAdvertisement();

        isReloading = true;
        StopCoroutine(LevelReloadDelay());
        StartCoroutine(LevelReloadDelay());
    }

    //reload from double tap input
    public void DoubleTapReload()
    {
        if (isReloading)
            return;

        UIManager.Instance.WhitePanelShow(true);
        LevelReload();
    }

    IEnumerator LevelReloadDelay()
    {

        isReloading = true;
        yield return new WaitForSeconds(timeDelay);
        UIManager.Instance.GameHUDMenuShow(true);
        LevelSetup();

    }

    public void GameManagerSetup(GameObject obj)
    {
        levelScr = obj.GetComponent<LevelManager>();
        UIManager.Instance.GameHUDMenuShow(true);
    }

    public void GameSetup(GameObject spawnObj)
    {

        Vector3[] playerPos = new Vector3[levelScr.LevelConfigGet.playerSpawnPosition.Length];

        for (int i = 0; i < playerPos.Length; i++)
        {
            playerPos[i] = levelScr.LevelConfigGet.playerSpawnPosition[i].position;
        }

        SpawnManager.Instance.SpawnManagerSetup(playerPos);
        cameraShake = levelScr.LevelConfigGet.gameCamera.gameObject.GetComponent<CameraShake>();


        levelScr.Setup();
        ResetValues();

    }

    // reset game manager
    public void ResetValues()
    {
        levelStars = 0;
        playerStars = 0;

        SpawnManager.Instance.PlayerSpawn(0);
        playerEnergy = levelScr.LevelConfigGet.startBounces;
        levelsGoals = levelScr.LevelConfigGet.levelTarget.Length;

        UIManager.Instance.PlayerEnergySetup(playerEnergy);
        UIManager.Instance.WhitePanelShow(false);

        StartCoroutine(GameplayDelay());
    }

    //switch to gamePlay state time delay 
    IEnumerator GameplayDelay()
    {
        yield return new WaitForSeconds(timeDelay);
        state = gameState.gamePlay;
        isReloading = false;
    }

    public void LevelSetup()
    {
        ResetValues();
        levelScr.Reset();          
    }

    public void LevelStarsUpdate()
    {
        levelStars--;
        playerStars++;
    }

    public void PlayerEnergyUpdate(bool increase)
    {
        if (increase)
        {
            playerEnergy++;
        }
        else
        {
            playerEnergy--;

            if (playerEnergy < 0)
                LevelFailed();
        }

        SpawnManager.Instance.PlayerEnergyUpdate(increase);
    }


    public void ShowLeaderboard()
    {
        if (OnLeaderboard != null)
            OnLeaderboard();
    }

    public void RateMe()
    {
        if (OnRateMe != null)
            OnRateMe();
    }

    public void PostScore(int score)
    {
        if (OnPostScores != null)
            OnPostScores(score);
    }

    public void UnlockArchivment()
    {
        if (OnUnlockArchivment != null)
            OnUnlockArchivment();
    }
}