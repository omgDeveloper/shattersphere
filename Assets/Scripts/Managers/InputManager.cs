﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//user inputs

public class InputManager : MonoBehaviour {

    public static InputManager Instance
    {
        get
        {
            if (s_Instance != null)
                return s_Instance;

            s_Instance = FindObjectOfType<InputManager>();

            if (s_Instance != null)
                return s_Instance;

            Create();

            return s_Instance;
        }
    }

    public static void Create()
    {
        InputManager controllerPrefab = Resources.Load<InputManager>("InputManager");
        s_Instance = Instantiate(controllerPrefab);
    }

    protected static InputManager s_Instance;

    private int touchMask = (1 << 8);

    Vector2 m_StartPos;
    public Vector2 m_StartPosGet { get { return m_StartPos; } }

    Vector3 screenTr;
    public Vector3 JoyPositionGet { get { return screenTr; } }

    Vector3 screenM_StartPos;
    public Vector3 JoyStartPositionGet { get { return screenM_StartPos; } }

    bool mouseDragging;
    public bool isDraggingGet { get { return mouseDragging; } }

    bool mouseDown;
    public bool isMouseDownGet { get { return mouseDown; } }

    GameObject hitObj;
    PlayerController selectedPlayer;
    public PlayerController SelectedPlayerGetSet { get { return selectedPlayer; } set { selectedPlayer = value; } }

    bool isAnyTouch;

    TKAnyTouchRecognizer anyTouchRecognizer;
    TKPanRecognizer panRecognizer;
    TKTapRecognizer tapRecognizer;

    private bool isTap;
    private float oneTapTime;
    private float secondTapTime;

    void Awake()
    {
        this.transform.parent = null;

        if (Instance != this)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {

        if (panRecognizer == null)
        {
            PanInput();
        }
        if (anyTouchRecognizer == null)
        {
            AnyTouch();
        }
        if (tapRecognizer == null)
        {
            TapInput();
        }

    }

    public void InputReset()
    {
        m_StartPos = Vector2.zero;
        screenTr = Vector3.zero;
        screenM_StartPos = Vector3.zero;
        mouseDragging = false;
        mouseDown = false;
        isAnyTouch = false;
        hitObj = null;
    }

    
    void TapInput()
    {
        var recognizer = new TKTapRecognizer();

        recognizer.gestureRecognizedEvent += (r) =>
        {
            // reloading level by dubble tap
            if (GameManager.Instance.gameStateGetSet == GameManager.gameState.gamePlay)
            {
                if (isTap)
                {
                    oneTapTime = Time.time;
                    isTap = false;

                    if (oneTapTime - secondTapTime < 0.5f) 
                    {
                        GameManager.Instance.DoubleTapReload();
                    }
                }
                else
                {
                    isTap = true;
                    secondTapTime = Time.time;
                }
            }
        };

        //recognizer.gestureRecognizedEvent += r =>
        //{

        //};

        TouchKit.addGestureRecognizer(recognizer);
    }

    //player dragging
    void PanInput()
    {
        panRecognizer = new TKPanRecognizer(0.75f);
        //dragging player
        panRecognizer.gestureRecognizedEvent += (r) =>
        {
            if (GameManager.Instance.gameStateGetSet == GameManager.gameState.gamePlay)
            {
                if (selectedPlayer && !hitObj && !isAnyTouch)
                {
                    if (!mouseDown)
                        screenM_StartPos = GetWorldPositionOnPlane(r.startPoint, 0);
                        screenTr = GetWorldPositionOnPlane(r.touchLocation(), 0);
            

                    mouseDown = true;
                    mouseDragging = true;
                    selectedPlayer.IsDraggingSet = true;
                }
            }
        };

        panRecognizer.gestureCompleteEvent += r =>
        {
            //release dragged player
            if (GameManager.Instance.gameStateGetSet == GameManager.gameState.gamePlay)
            {
                if (selectedPlayer && !hitObj && !isAnyTouch)
                {
                    mouseDown = false;
                    selectedPlayer.IsDraggingSet = false;
                    selectedPlayer.ForceRelasedSet = true;
                }
                else
                    hitObj = null;
            }
        };
        TouchKit.addGestureRecognizer(panRecognizer);
    }

    void AnyTouch()
    {
        anyTouchRecognizer = new TKAnyTouchRecognizer(new TKRect(0, 0, Screen.width, Screen.height));

        //player, touchable object select
        anyTouchRecognizer.onEnteredEvent += (r) =>
        {
            if (GameManager.Instance.gameStateGetSet == GameManager.gameState.gamePlay)
            {
                Vector2 pos = r.touchLocation();
                Ray ray = GameManager.Instance.GameCamera().ScreenPointToRay(pos);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 1000f))
                {
                    if (hit.collider.gameObject.tag == "Touchable")
                    {
                        isAnyTouch = true;
                        ObjectBase objBase = hit.collider.gameObject.GetComponent<ObjectBase>();
                        objBase.TouchObjectAction();        
                    }
                    else if(hit.collider.gameObject.tag == "Player")
                    {
                        if (selectedPlayer)
                            selectedPlayer.UnSelectPlayer();

                        selectedPlayer = hit.collider.gameObject.GetComponent<PlayerController>();
                        selectedPlayer.SelectPlayer();
                        isAnyTouch = false;
                    }

                }
                else
                {
                    hitObj = null;
                    isAnyTouch = false;
                }
            }
        };

        anyTouchRecognizer.onExitedEvent += r =>
        {
            if(isAnyTouch)
                isAnyTouch = false;
        };

        TouchKit.addGestureRecognizer(anyTouchRecognizer);
    }

    
    Vector3 GetWorldPositionOnPlane(Vector3 screenPosition, float z)
    {
        Ray ray = GameManager.Instance.GameCamera().ScreenPointToRay(screenPosition);
        Plane xy = new Plane(Vector3.forward, new Vector3(0, 0, z));
        float distance;
        xy.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }

}
