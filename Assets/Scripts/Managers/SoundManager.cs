﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

//sound fx and music pooling controll

public enum SOUND
{
    defaultBounce,
    playerShatter,
    levelEnd
}


public class SoundManager : MonoBehaviour 
{

    public static SoundManager Instance
    {
        get
        {
            if (s_Instance != null)
                return s_Instance;

            s_Instance = FindObjectOfType<SoundManager>();

            if (s_Instance != null)
                return s_Instance;

            Create();

            return s_Instance;
        }
    }
    public static void Create()
    {
        SoundManager controllerPrefab = Resources.Load<SoundManager>("SoundManager");
        s_Instance = Instantiate(controllerPrefab);
    }
    protected static SoundManager s_Instance;

    private bool isSoundFX;
    private bool isMusic;

    public AudioClip[] soundsFx;
    public AudioClip[] ambientMusic;

    private SoundKit.SKSound currentAmbientAudio;

    private bool isFading;

    private void Awake()
    {
        this.transform.parent = null;

        if (Instance != this)
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        
        SoundsSettingsUpdate();
    }

    public void JustPlay()
    {
        if (isMusic)
        {
            int musicIndex = Random.Range(0, ambientMusic.Length);
            SoundKit.instance.playBackgroundMusic(ambientMusic[musicIndex], 1f,true);
        }
    }

    public void StopStartAmbientMusic(bool stopping)
    {
        SoundsSettingsUpdate();

        StartCoroutine(StopStartAmbientDelay(stopping));
    }

    IEnumerator StopStartAmbientDelay(bool stopping)
    {
        yield return new WaitForEndOfFrame();
    
        if (stopping)
        {
            if (currentAmbientAudio != null)
            {
                currentAmbientAudio.fadeOutAndStop(1f);
                isFading = true;
            }
        }
        else
        {
            PlayMusic();
        }
    }

    public void PlayMusic()
    {
        if(isMusic)
        {
            if (isFading)
                return;

            if (currentAmbientAudio != null)
                currentAmbientAudio = null;

            int musicIndex = Random.Range(0, ambientMusic.Length);
            currentAmbientAudio = null;
            AudioMixerGroup mixer = SoundKit.instance.backgMusic;

            currentAmbientAudio = SoundKit.instance.playSound(ambientMusic[musicIndex], 0.5f).setCompletionHandler(AmbientEnd);
        }
    }

    private void AmbientEnd()
    {
        isFading = false;
        PlayMusic();
    }

    public void SoundsSettingsUpdate()
    {
        isMusic = SavedDataManager.Instance.GameSettingsGetSet.musicFx;
        isSoundFX = SavedDataManager.Instance.GameSettingsGetSet.soundFx;
    }


    public void PlaySoundFx(SOUND soundToPlay, float volume = 1f)
    {

        if (isSoundFX)
        {
            SoundKit.instance.playOneShot(soundsFx[(int)soundToPlay], volume);
        }
    }

    public void PlayBounceSoundFx(SOUND soundToPlay, float pitch = 1f)
    {

        if (isSoundFX)
        {
            SoundKit.instance.playOneShotBallBounce(soundsFx[(int)soundToPlay], pitch);         
        }
    }

}
