﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapToScreen : MonoBehaviour {

    private enum SnapTo
    {
        cornerTopLeft,
        cornerTopRight,
        cornerBottomLeft,
        cornerBottomRight,
        left,
        right,
        top,
        bottom

    }
    [SerializeField] private SnapTo snap;

    private Camera gameCamera;
    private float plane = 40f;

    private void Start()
    {
        gameCamera = Camera.main;

        if (snap == SnapTo.cornerTopRight)
            transform.position = gameCamera.ViewportToWorldPoint(new Vector3(1f, 1f, plane));
        else if (snap == SnapTo.cornerTopLeft)
            transform.position = gameCamera.ViewportToWorldPoint(new Vector3(0, 1f, plane));
        else if (snap == SnapTo.cornerBottomLeft)
            transform.position = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, plane));
        else if (snap == SnapTo.cornerBottomRight)
            transform.position = gameCamera.ViewportToWorldPoint(new Vector3(1, 0, plane));


        if (snap == SnapTo.left)
        {
            Vector3 pos = gameCamera.ViewportToWorldPoint(new Vector3(0, 1f, plane));
            transform.position = new Vector3(pos.x, transform.position.y, transform.position.z);
        }
        else if (snap == SnapTo.right)
        {
            Vector3 pos = gameCamera.ViewportToWorldPoint(new Vector3(1f, 1f, plane));
            transform.position = new Vector3(pos.x, transform.position.y, transform.position.z);
        }
        else if (snap == SnapTo.top)
        {
            Vector3 pos = gameCamera.ViewportToWorldPoint(new Vector3(1f, 1f, plane));
            transform.position = new Vector3(transform.position.x, pos.y, transform.position.z);
        }
        else if (snap == SnapTo.bottom)
        {
            Vector3 pos = gameCamera.ViewportToWorldPoint(new Vector3(1f, 1f, plane));
            transform.position = new Vector3(transform.position.x, pos.y, transform.position.z);
        }

    }
}
