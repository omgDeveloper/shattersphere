﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Prime31.ZestKit;

public class GameHUD : MonoBehaviour {

    public static GameHUD Instance
    {
        get
        {
            if (s_Instance != null)
                return s_Instance;

            s_Instance = FindObjectOfType<GameHUD>();

            if (s_Instance != null)
                return s_Instance;

            Create();

            return s_Instance;
        }
    }
    public static void Create()
    {
        GameHUD controllerPrefab = Resources.Load<GameHUD>("GameHudUI");
        s_Instance = Instantiate(controllerPrefab);
    }

    protected static GameHUD s_Instance;

    private CanvasGroup gameHudPanel;
    private CanvasGroup loadingPanel;
    private CanvasGroup levelCompletedPanel;
    private Image[] levelCompleteStars;
    private CanvasGroup whitePanel;

    public GameObject bouncesPrefab;

    private Text shootsNumber;

    ///  Tweens
    public Spline spline;
    ITween<Vector2> levelAnim;

    TweenChain levelNumberShow;
    TweenChain levelNumberHide;


    void UiInitialization()
    {
        GameHUDMenuConfig();

    }


    #region Game HUD Config and Show ##########################################################################################

    void GameHUDMenuConfig()
    {
        gameHudPanel = transform.Find("GameHudPanel").gameObject.GetComponent<CanvasGroup>();

        Button hudBackMMBtn = gameHudPanel.transform.Find("BackMM").gameObject.GetComponent<Button>();
        hudBackMMBtn.onClick.AddListener(delegate
        {
           // GameHUDMenuShow(false);
           // MainMenuShow(true);
          //  gameMgr.UnloadActiveLevel();

          //  ProbeControllerShow(false);
            
        });

        Button reloadLevelBtn = gameHudPanel.transform.Find("ReloadLevel").gameObject.GetComponent<Button>();
        reloadLevelBtn.onClick.AddListener(delegate
        {
           
          //  WhitePanelShow(true);
          //  gameMgr.LevelReload();

        });

    }


    public void EggEnergySetup(int energy)
    {
        Transform eggEnergyPanel = gameHudPanel.transform.Find("EnergyLayout").transform;
       // gameMgr.levelRootObjGet.GetComponent<SpawnManager>().EggEnergySetup(eggEnergyPanel, energy);
    }


    public void GameHUDMenuShow(bool show)
    {
        if (show)
        {
            gameHudPanel.alpha = 1f;
        }
        else
        {
            gameHudPanel.alpha = 0;
        }

        gameHudPanel.blocksRaycasts = show;
    }

    #endregion

    #region Loading Screen Config and Show ##########################################################################################

    void LoadingScreenConfig()
    {
        loadingPanel = transform.Find("LoadingScreenPanel").gameObject.GetComponent<CanvasGroup>();
    }

    public void LoadingScreenShow(bool show)
    {
        LevelAnim(show);
    }

    public void LoadedLevel(string level)
    {
        loadingPanel.transform.Find("LoadLevel").gameObject.GetComponent<Text>().text = level;
    }


    void LevelAnim(bool show)
    {
        RectTransform levels = loadingPanel.transform.Find("LoadLevel").gameObject.GetComponent<RectTransform>();
        Image loadingImage = loadingPanel.transform.Find("Image").gameObject.GetComponent<Image>();


        if (show)
        {
            LevelShowCanvas(true);
            levels.anchoredPosition = new Vector2(0, Screen.height * -2);
            loadingImage.color = new Color(loadingImage.color.r, loadingImage.color.g, loadingImage.color.b, 0);
            //var levelNumberShow = new TweenChain();
            //levelNumberShow.appendTween(loadingImage.ZKalphaTo(1f, 0.25f).setEaseType(EaseType.QuintIn))
            //           .appendTween(levels.ZKanchoredPositionTo(Vector2.zero, 0.5f).setEaseType(EaseType.QuintOut).setDelay(0.2f)
            //           .setCompletionHandler(t => LevelMapShow(false)))
            //           .start();
        }
        else
        {
            GameHUDMenuShow(true);
            //LevelCompletedShow(false, 0);
            //var levelNumberHide = new TweenChain();
            //levelNumberHide.appendTween(levels.ZKanchoredPositionTo(new Vector2(0, Screen.height * 2), 0.5f).setEaseType(EaseType.ElasticIn))
            //.appendTween(loadingImage.ZKalphaTo(0f, 0.5f).setEaseType(EaseType.QuintOut))
            //.setCompletionHandler(t => LevelShowCanvas(false))
            //.start();
        }
    }

    void LevelShowCanvas(bool show)
    {
        if (show)
        {
          //  gameMgr.gameStateGetSet = GameManager.gameState.loading;
            loadingPanel.alpha = 1f;
        }
        else
        {
            loadingPanel.alpha = 0f;
            GameHUDMenuShow(true);
        }
        loadingPanel.blocksRaycasts = show;
    }


    #endregion
}
