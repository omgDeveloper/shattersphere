﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuPanel : MonoBehaviour, IguiPanel
{
    private CanvasGroup panelCanvas;
    [SerializeField] private Button playBtn;

    [SerializeField] private Toggle musicToggle;
    [SerializeField] private Toggle soundToggle;

    [SerializeField] private Toggle autoLoadToggle;

    [SerializeField] private Button hiFiveBtn;
    [SerializeField] private Button highScoreBtn;

  

    public void Initialization()
    {
        panelCanvas = GetComponent<CanvasGroup>();

        playBtn.onClick.AddListener(delegate
        {
            UIManager.Instance.LevelMapShow(true);
            UIManager.Instance.MainMenuShow(false);
        });

        musicToggle.onValueChanged.AddListener(delegate
        {

            SavedDataManager.Instance.GameSettingsGetSet.musicFx = musicToggle.isOn;
            SavedDataManager.Instance.SaveGameSettingsData();

            SoundManager.Instance.StopStartAmbientMusic(!musicToggle.isOn);

        });
        musicToggle.isOn = SavedDataManager.Instance.GameSettingsGetSet.musicFx;

        soundToggle.onValueChanged.AddListener(delegate
        {
            SavedDataManager.Instance.GameSettingsGetSet.soundFx = soundToggle.isOn;
            SavedDataManager.Instance.SaveGameSettingsData();
        });
        soundToggle.isOn = SavedDataManager.Instance.GameSettingsGetSet.soundFx;

        autoLoadToggle.onValueChanged.AddListener(delegate
        {
            SavedDataManager.Instance.GameSettingsGetSet.autoLoad = autoLoadToggle.isOn;
            SavedDataManager.Instance.SaveGameSettingsData();
        });
        autoLoadToggle.isOn = SavedDataManager.Instance.GameSettingsGetSet.autoLoad;

        hiFiveBtn.onClick.AddListener(delegate
        {
            GameManager.Instance.RateMe();
        });

        highScoreBtn.onClick.AddListener(delegate
        {
            GameManager.Instance.ShowLeaderboard();
        });

    }


    public void Show()
    {
        UIManager.Instance.CanvasGroupAnimationShow(panelCanvas);

        GameManager.Instance.gameStateGetSet = GameManager.gameState.mainMenu;
    }

    public void Hide()
    {
        UIManager.Instance.CanvasGroupAnimationHide(panelCanvas);
    }
}
