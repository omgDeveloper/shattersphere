﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class LevelSelectPanel : MonoBehaviour, IguiPanel
{
     private CanvasGroup panelCanvas;

    [SerializeField] private Button backMMBtn;

    [SerializeField] private Episode[] episodes;
    [SerializeField] private int allEpisodes = 3;
    [SerializeField] private int levelsInEpisode = 16;

    [SerializeField] private Transform contentTr;

    [SerializeField] private Sprite[] reachedStar;
    [SerializeField] private Toggle[] ePagination;

    [SerializeField] private Transform paginationParentTr;

    [SerializeField] private GameObject levelBuittonPrefab;
    [SerializeField] private GameObject episodesPrefab;
    [SerializeField] private GameObject episodesPaginationPrefab;

    [SerializeField] private ScrollSnap scrolSnap;

    public void Initialization()
    {
        panelCanvas = GetComponent<CanvasGroup>();

        backMMBtn.onClick.AddListener(delegate
        {
            UIManager.Instance.MainMenuShow(true);
            UIManager.Instance.LevelMapShow(false);
        });

        scrolSnap.onLerpComplete.AddListener(OnSnapComplete);

        ScroolInit();
    }

    public void Show()
    {
        UpdateLevelsElements();

        UIManager.Instance.CanvasGroupAnimationShow(panelCanvas);

        GameManager.Instance.gameStateGetSet = GameManager.gameState.levelSelect;
    }

    public void Hide()
    {
        UIManager.Instance.CanvasGroupAnimationHide(panelCanvas);
    }

    private void OnSnapComplete()
    {
        PaginationUpdate();
    }

    private void ScroolInit()
    {
        episodes = new Episode[allEpisodes];

        int levelIndex = 1;

        for (int i = 0; i < episodes.Length; i++)
        {
            EpisodeElementInstantiate(i);

            for (int x = 0; x < levelsInEpisode; x++)
            {
                LevelElementInstantiate(i, x);

                AddLevelListener(episodes[i].levels[x], levelIndex);

                levelIndex++;
            }
        }

        EpisodesPaginationInstantiate();
    }

    private void EpisodeElementInstantiate(int episodeIndex)
    {
        GameObject episode = Instantiate(episodesPrefab, Vector3.zero, Quaternion.identity) as GameObject;

        episode.transform.parent = contentTr;

        episode.transform.localPosition = Vector3.zero;
        episode.transform.localScale = Vector3.one;
        episode.name = "Episode" + episodeIndex;

        float screenWidth = contentTr.GetComponent<RectTransform>().rect.width;

        episode.GetComponent<LayoutElement>().preferredWidth = screenWidth;

        episodes[episodeIndex] = new Episode();

        string episodeLabel = (episodeIndex + 1).ToString();
        episode.GetComponent<EpisodeUiElement>().Setup(episodeLabel);

        episodes[episodeIndex].levels = new Button[levelsInEpisode];
        episodes[episodeIndex].stars = new int[levelsInEpisode];
    }

    private void LevelElementInstantiate(int episodeIndex, int levelIndex)
    {
        GameObject btnObj = Instantiate(levelBuittonPrefab, Vector3.zero, Quaternion.identity) as GameObject;
        btnObj.transform.parent = contentTr.Find("Episode" + episodeIndex + "/LevelsGrid").transform;

        btnObj.transform.localPosition = Vector3.zero;
        btnObj.transform.localScale = Vector3.one;
        btnObj.name = "level" + levelIndex;

        episodes[episodeIndex].levels[levelIndex] = btnObj.GetComponent<Button>();       
    }

    private void EpisodesPaginationInstantiate()
    {
        ePagination = new Toggle[episodes.Length];
        for (int j = 0; j < ePagination.Length; j++)
        {
            GameObject pagin = Instantiate(episodesPaginationPrefab, Vector3.zero, Quaternion.identity) as GameObject;
            pagin.transform.parent = paginationParentTr;

            pagin.transform.localPosition = Vector3.zero;
            pagin.transform.localScale = Vector3.one;
            pagin.name = "ep" + j;
            ePagination[j] = pagin.GetComponent<Toggle>();
            ePagination[j].group = paginationParentTr.GetComponent<ToggleGroup>();
        }

        paginationParentTr.GetComponent<GridLayoutGroup>().constraintCount = episodes.Length;

        ePagination[0].isOn = true;
    }

    private void UpdateLevelsElements()
    {
        int levelIndex = 1;
        for (int i = 0; i < episodes.Length; i++)
        {

            for (int x = 0; x < levelsInEpisode; x++)
            {
                bool isUnlock = SavedDataManager.Instance.GameProgresGetSet.isLevelUnlocked[levelIndex];
                int stars = SavedDataManager.Instance.GameProgresGetSet.stars[levelIndex];

                LevelUiElement element = episodes[i].levels[x].GetComponent<LevelUiElement>();
                element.UpdateElement(isUnlock, stars, levelIndex);

                levelIndex++;
            }

        }

    }

    private void AddLevelListener(Button btn, int level)
    {
        btn.onClick.AddListener(() => LoadLevelScene(level, btn.transform));
    }

    private void LoadLevelScene(int levelIndex, Transform btnTr)
    {
        //btnTr.transform.ZKlocalScaleTo(Vector3.one * 1.25f, 0.25f).setLoops(LoopType.PingPong).start();

        UIManager.Instance.LoadingScreenShow(true);

        StartCoroutine(LoadLevelDelay(levelIndex));
    }

    IEnumerator LoadLevelDelay(int index)
    {

        yield return new WaitForSeconds(0.5f);
        ScenesLoader.Instance.LoadLevelScene(index);
    }

    private void PaginationUpdate()
    {
        int currentIndex = scrolSnap.CurrentIndex;

        ePagination[currentIndex].isOn = true;
    }

 
}
