﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EpisodeUiElement : MonoBehaviour
{
    [SerializeField] private Text digits;
    [SerializeField] private GridLayoutGroup grid;

    public void Setup(string digit)
    {
        digits.text = digit;
    }
}
