﻿using Prime31.ZestKit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EditorExtensions;

public class LoadingScreenPanel : MonoBehaviour, IguiPanel
{
     private CanvasGroup panelCanvas;
    [SerializeField] private Text levelNameTxt;
    [SerializeField] private RectTransform levels;
    [SerializeField] private Image loadingBckg;

    [SerializeField] private GameObject starsPanel;

    [SerializeField] private Image[] loadingStars;
    [SerializeField] private Sprite[] completeStars;

    [SerializeField] private Animation animation;


    private int GetReachedStars()
    {
        return GameManager.Instance.PlayerActualStarsGetSet - 1;
    }

    private string GetLevelForLoadName()
    {
        string name = "MainMenu";
        int actualLevelIndex = ScenesLoader.Instance.ActualLevelIndexGet;

        if (actualLevelIndex > 0)
            name = actualLevelIndex.ToString();

        return name;
    }

    public void Initialization()
    {
        panelCanvas = GetComponent<CanvasGroup>();
    }

    public void Hide()
    {
        panelCanvas.alpha = 1f;
        levels.anchoredPosition = Vector2.zero;

        var levelNumberHide = new TweenChain();

        levelNumberHide.appendTween(levels.ZKanchoredPositionTo(new Vector2(0, Screen.height * 1.5f), 0.75f).setEaseType(EaseType.QuintIn).setCompletionHandler(t => OnCanvasShowComplete()))
        .appendTween(panelCanvas.ZKalphaTo(0, 0.25f).setDelay(0.25f))
        .start();

        if (starsPanel.activeSelf)
        {
            StartCoroutine(LoadingLevelReachedStars(GetReachedStars(), false));
        }
    }

    public void OnCanvasShowComplete()
    {
      ScenesLoader.Instance.ActivateScene();
    }

    public void Show()
    {
        levelNameTxt.text = GetLevelForLoadName();


        levels.anchoredPosition = new Vector2(0, Screen.height * -1.5f);

        var levelNumberShow = new TweenChain();

        levelNumberShow.appendTween(panelCanvas.ZKalphaTo(1, 0.25f))
                   .appendTween(levels.ZKanchoredPositionTo(Vector2.zero, 0.5f).setEaseType(EaseType.QuintOut).setDelay(0.2f))               
                   .start();

        starsPanel.SetActive(false);

        int reachedStars = GetReachedStars();

        if (reachedStars >= 0)//(reachedStars >= 0)
            StartCoroutine(LoadingLevelReachedStars(reachedStars, true));


        GameManager.Instance.gameStateGetSet = GameManager.gameState.loading;
    }

    public void PlayStopLoadingAnim(bool isPlay)
    {
        if (isPlay)
            animation.Play();
        else
            animation.Stop();
    }

    IEnumerator LoadingLevelReachedStars(int stars, bool show)
    {
        if (show)
        {
            starsPanel.SetActive(true);

            foreach (var star in loadingStars)
            {
                star.sprite = completeStars[0];
            }

            for (int i = 0; i < loadingStars.Length; i++)
            {
                yield return new WaitForSeconds(0.1f);
                if(i<=stars)
                loadingStars[i].sprite = completeStars[1];
                RectTransform rec = loadingStars[i].GetComponent<RectTransform>();
                rec.localScale = Vector2.one * 0.25f;
                rec.ZKlocalScaleTo(Vector3.one, 0.75f).setEaseType(EaseType.ElasticOut).start();
            }
        }
        else
        {
            float animTime = 0.075f;
            for (int i = loadingStars.Length - 1; i >-1; i--)
            {
                yield return new WaitForSeconds(0.1f);
                RectTransform rec = loadingStars[i].GetComponent<RectTransform>();
                rec.ZKlocalScaleTo(Vector3.zero, animTime).setEaseType(EaseType.BackIn).start();
            }

            yield return new WaitForSeconds(animTime * (loadingStars.Length - 1));
            starsPanel.SetActive(false);
        }

        
        yield return null;
    }
}
