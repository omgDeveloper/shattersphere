﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31.ZestKit;

public class WhitePanel : MonoBehaviour, IguiPanel
{
    private CanvasGroup panelCanvas;

    public void Initialization()
    {
        panelCanvas = GetComponent<CanvasGroup>();
    }

    public void Hide()
    {
        panelCanvas.ZKalphaTo(0, 0.25f).setCompletionHandler(t => WhitePanelCompletion()).start();
    }

  
    public void Show()
    {
        panelCanvas.blocksRaycasts = true;
        panelCanvas.ZKalphaTo(1f, 0.25f).start();
    }


    void WhitePanelCompletion()
    {
        panelCanvas.blocksRaycasts = false;
    }
}
