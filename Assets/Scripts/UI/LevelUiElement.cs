﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelUiElement : MonoBehaviour
{
    [SerializeField] private Button button;
    [SerializeField] private CanvasGroup panelCanvas;
    [SerializeField] private Text digits;
    [SerializeField] private Image[] stars;

    [SerializeField] private Sprite[] starSprites;

     public void UpdateElement(bool enable, int reachedStars, int levelNumber)
    {
        button.interactable = enable;

        digits.text = levelNumber.ToString();

        digits.enabled = enable;


        if (enable)
            panelCanvas.alpha = 1f;
        else
            panelCanvas.alpha = 0f;

        for (int i = 0; i < reachedStars; i++)
        {
            if (i <= reachedStars)
                stars[i].sprite = starSprites[1];
            else
                stars[i].sprite = starSprites[0];
        }
        
    }

}
