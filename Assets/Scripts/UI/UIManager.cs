﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Prime31.ZestKit;

// UI show/hide screens

public class UIManager : MonoBehaviour
{

    public static UIManager Instance
    {
        get
        {
            if (s_Instance != null)
                return s_Instance;

            s_Instance = FindObjectOfType<UIManager>();

            if (s_Instance != null)
                return s_Instance;

            Create();

            return s_Instance;
        }
    }
    public static void Create()
    {
        UIManager controllerPrefab = Resources.Load<UIManager>("GameUI");
        s_Instance = Instantiate(controllerPrefab);
    }
    protected static UIManager s_Instance;

    [SerializeField] private MainMenuPanel mainMenuPanel;
    [SerializeField] private LevelSelectPanel levelSelectPanel;
    [SerializeField] private GameHudPanel gameHudPanel;
    [SerializeField] private LevelCompletedPanel levelCompletePanel;
    [SerializeField] private LoadingScreenPanel loadingScreenPanel;
    [SerializeField] private WhitePanel whitePanel;

    [SerializeField] private CanvasGroup whiteScreenPanel;

    private int allEpisodes = 3;
    private int levelsInEpisode = 16;

    public int levelsInEpisodesGet { get { return levelsInEpisode; } }
    public int allEpisodesLenghtGet { get { return allEpisodes; } }

    void Awake()
    {
        if (Instance != this)
        {
            Destroy(gameObject);
            return;
        }
    }

    void Start()
    {
        UiInitialization();
    }

    void UiInitialization()
    {

        IguiPanel[] childs = GetComponentsInChildren<IguiPanel>(true);

        foreach (var uiPanel in childs)
        {
            uiPanel.Initialization();
        }

        if (GameManager.Instance.gameStateGetSet == GameManager.gameState.mainMenu)
            mainMenuPanel.Show();
    }

    public void MainMenuShow(bool show)
    {
        if(show)
            mainMenuPanel.Show();
        else
            mainMenuPanel.Hide();
    }

    public void LoadingScreenShow(bool show)
    {
        if (show)
            loadingScreenPanel.Show();
        else
            loadingScreenPanel.Hide();
    }

    public void GameHUDMenuShow(bool show)
    {
        if (show)
            gameHudPanel.Show();
        else
            gameHudPanel.Hide();
    }

    public void LevelMapShow(bool show)
    {
        if (show)
            levelSelectPanel.Show();
        else
            levelSelectPanel.Hide();
    }

    public void LevelCompletedShow(bool show, int stars)
    {
        if (show)
            levelCompletePanel.Show();
        else
            levelCompletePanel.Hide();
    }

    public void WhitePanelShow(bool show)
    {
        if (show)
            whitePanel.Show();
        else
            whitePanel.Hide();
    }

    public void PlayerEnergySetup(int startEnergy)
    {
        gameHudPanel.StartEnergyInitialization(startEnergy);
    }

    public void CanvasGroupAnimationShow(CanvasGroup canvas)
    {
        canvas.ZKalphaTo(1f, 0.5f)
            .setCompletionHandler(t => OnCanvasShowComplete(canvas))
            .start();
    }

    public void OnCanvasShowComplete(CanvasGroup canvas)
    {
        canvas.blocksRaycasts = true;
    }

    public void CanvasGroupAnimationHide(CanvasGroup canvas)
    {
        canvas.ZKalphaTo(0, 0.5f)
            .setCompletionHandler(t => OnCanvasHideComplete(canvas))
            .start();
    }

    public void OnCanvasHideComplete(CanvasGroup canvas)
    {
        canvas.blocksRaycasts = false;
    }

}
