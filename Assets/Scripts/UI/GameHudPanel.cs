﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameHudPanel : MonoBehaviour, IgameHUD
{
    private CanvasGroup panelCanvas;
   [SerializeField] private Button hudBackMMBtn;
   [SerializeField] private Button reloadLevelBtn;
    [SerializeField] private Transform parentTr;


    public void Initialization()
    {
        panelCanvas = GetComponent<CanvasGroup>();

        hudBackMMBtn.onClick.AddListener(delegate
        {
            if (
                GameManager.Instance.state == GameManager.gameState.levelCompleted ||
                GameManager.Instance.state == GameManager.gameState.levelFailed
              )
                return;

            GameManager.Instance.LoadMainMenu();

        });

        reloadLevelBtn.onClick.AddListener(delegate
        {
            if (GameManager.Instance.IsReloadingGet)
                return;

            UIManager.Instance.WhitePanelShow(true);
            GameManager.Instance.LevelReload();
        });

    }

    public void Hide()
    {
        UIManager.Instance.CanvasGroupAnimationHide(panelCanvas);
    }

    public void Show()
    {
        UIManager.Instance.CanvasGroupAnimationShow(panelCanvas);
    }

    public void StartEnergyInitialization(int startEnergy)
    {
        SpawnManager.Instance.PlayerEnergySetup(parentTr, startEnergy);
    }

}
