﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelCompletedPanel : MonoBehaviour, IguiPanel
{
    private CanvasGroup panelCanvas;
    [SerializeField] private Button homeBtn;
    [SerializeField] private Button reloadBtn;
    [SerializeField] private Button nextBtn;

    [SerializeField] private Image[] reachedStars;

    public void Initialization()
    {
        panelCanvas = GetComponent<CanvasGroup>();

        homeBtn.onClick.AddListener(delegate
        {
            GameManager.Instance.LoadMainMenu();
        });

        reloadBtn.onClick.AddListener(delegate
        {
            GameManager.Instance.LevelReload();
        });

        nextBtn.onClick.AddListener(delegate
        {
            ScenesLoader.Instance.LoadNextLevel();
        });

    }

    public void Show()
    {

    }

    public void Hide()
    {

    }
}
