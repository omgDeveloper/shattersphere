﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    private Transform tr;
    Vector3 oryginalPos;
    public bool debugMode = false;

    public float shakeAmount;
    public float shakeDuration;


    float shakePercentage;
    float startAmount;
    float startDuration;

    bool isRunning = false; 

    public bool smooth;
    public float smoothAmount = 5f;

    void Start()
    {
        tr = this.gameObject.GetComponent<Transform>();
        oryginalPos = tr.localPosition;
        if (debugMode) ShakeCamera();
    }


    void ShakeCamera()
    {

        startAmount = shakeAmount;
        startDuration = shakeDuration;

        if (!isRunning) StartCoroutine(Shake());
    }

    public void ShakeCamera(float amount, float duration)
    {

        shakeAmount = amount;
        startAmount = shakeAmount;
        

        if (!isRunning)
        {
            shakeDuration = duration;
            startDuration = shakeDuration;
            StartCoroutine(Shake());
        }
    }


    IEnumerator Shake()
    {
        isRunning = true;

        while (shakeDuration > 0.01f)
        {
            Vector3 rotationAmount = Random.insideUnitSphere * shakeAmount;
            rotationAmount.z = oryginalPos.z;

            shakePercentage = shakeDuration / startDuration;

            shakeAmount = startAmount * shakePercentage;
            shakeDuration = Mathf.Lerp(shakeDuration, 0, Time.deltaTime);

            if (smooth)
                tr.localPosition = Vector3.Lerp(tr.localPosition, rotationAmount, Time.deltaTime * smoothAmount);
            else
                tr.localPosition = rotationAmount;

            yield return null;
        }

        tr.localRotation = Quaternion.identity;
        tr.localPosition = oryginalPos;
        isRunning = false;
    }
}
