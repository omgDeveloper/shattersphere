﻿using UnityEngine;
using System;

public class Timer
{
    private Action timerCallback;
    private float timer;

    public float GetCurrentTime()
    {
        return timer;
    }

    public Timer (float timer, Action timerCallback)
    {
        this.timer = timer;
        this.timerCallback = timerCallback;
    }


    public void UpdateTimer()
    {
        if (IsTimeToCountdown())
        {
            timer -= Time.deltaTime;

            if (IsTimeComplete())
                timerCallback();
        }
    }

    private bool IsTimeToCountdown()
    {
        return timer > 0f;
    }

    private bool IsTimeComplete()
    {
        return timer <= 0;
    }
}
