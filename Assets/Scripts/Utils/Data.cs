﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//UI
[System.Serializable]
public class Episode
{
    public CanvasGroup episodeCanvas;
    public Button[] levels;
    public int[] stars;
    public bool isUnlock;
}

// Save/Load Data
[System.Serializable]
public class GameProgress
{
    public bool[] isLevelUnlocked;
    public int[] stars;
}

[System.Serializable]
public class GameSettings
{
    public bool soundFx;
    public bool musicFx;
    public bool autoLoad;
    public int lastlevel;
}

// Game Level data
[System.Serializable]
public class LevelConfigData
{
    public int startBounces = 3;
    public int maxShoots;
    public Transform[] playerSpawnPosition;
    public GameObject[] levelTarget;
    [HideInInspector]
    public Camera gameCamera;
    [HideInInspector]
    public ObjectBase[] interactiveObjects;
    [HideInInspector]
    public bool[] relasedPlayers;

}

