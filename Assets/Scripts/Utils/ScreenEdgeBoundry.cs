﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenEdgeBoundry : MonoBehaviour {

    public Transform right;
    public Transform left;
    public Transform top;
    public Transform bottom;
    public Camera gameCamera;
    public Transform sp;
    public float plane = 40f;

    void Start()
    {
        right.transform.position = gameCamera.ViewportToWorldPoint(new Vector3(1, 0.5f, plane));
        left.transform.position = gameCamera.ViewportToWorldPoint(new Vector3(0, 0.5f, plane));
        top.transform.position = gameCamera.ViewportToWorldPoint(new Vector3(0.5f, 1, plane));
        bottom.transform.position = gameCamera.ViewportToWorldPoint(new Vector3(0.5f, 0, plane));

        //right.transform.position = new Vector3(left.transform.position.x, left.transform.position.y, plane);
        //left.transform.position = new Vector3(left.transform.position.x, left.transform.position.y, 0);
        //top.transform.position = new Vector3(top.transform.position.x, top.transform.position.y, 0);
        //bottom.transform.position = new Vector3(bottom.transform.position.x, bottom.transform.position.y, 0);




    }

    }
