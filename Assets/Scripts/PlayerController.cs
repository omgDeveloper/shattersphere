﻿using Klak.Motion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//player controller

public class PlayerController : MonoBehaviour {

    Transform tr;
    Rigidbody rb;
    Collider coll;
    Vector3 startPos;
    public GameObject shatterPrefab;
    bool isActive; //can be selected
    public bool isActiveGetSet { get { return isActive; } set { isActive = value; } }
    bool isSelected;// is selected, can be dragging
    private int touchMask = (1 << 10);
    private bool isReleased; // is released from dragging, in motion, waiting for result
    private int playerIndex;//
    public int PlayerIndexGetSet { get { return playerIndex; } set { playerIndex = value; } }
    private bool isInTarget; //is in target
    public bool IsInTargetGetSet { get { return isInTarget; } set { isInTarget = value; } }

    //movement,force data
    public float magBase = 5;
    public float magMultiplier = 15;
    public float maxSpeed = 150f;
    private float minVel = 0.1f;
    private float maxRelativeVelPower = 2.5f;
    private float velocityFactor = 1f; // velocity weight
    public int velDir = 1;
    public int setVelocityDir { set { velDir = value; } }
    private float setVelSpeed = 1.25f;
    public int VelocitySpeedSet { set { setVelSpeed = value; } }

    public SnapDir snapDirection = SnapDir.away;
    private ForceMode forceTypeToApply = ForceMode.Impulse;

    public bool overrideVelocity = true; 
    public bool pauseOnDrag = true; 

    public enum SnapDir { toward, away }

    private Vector3 lastFrameVelocity;

    private Vector3 forceVector;
    private float magPercent = 0;
    private bool forceRelased = false;
    public bool ForceRelasedSet {set { forceRelased = value; } }
    bool isDragging = false;
    public bool IsDraggingSet {set { isDragging = value; } }

    private float dragDistance;
    private bool collIsDisabled;
    
    //trial, trajctory, visualisations
    private LineRenderer playerTargetLine;
    private LineRenderer playerTrajectory;
    public float timeResolution = 0.25f;
    public float maxTime = 5f;

    Material lRendererMaterial;
    public float scrollSpeed = 2.5f;
    public float pulseSpeed = 2.5f;
    public float noiseSize = 1.0f;
    public float maxWidth = 2.5f;
    public float minWidth = 0.5f;
    private float aniDir = 1.0f;
    private Transform playerArt;

    public ParticleSystem particlesFx;
    private ParticleSystem.MainModule particleMain;
    TrailRenderer playerTrail;


    private float posNoiseWeight = 0;
    private int posNoiseDir = 0;
    public int PosNoiseDirSet { set { posNoiseDir = value; } }
    private BrownianMotion noiseMotion; // noise motion component
    private Vector3 deathTarget;
    private bool isInDeathArea;

    public Gradient powerGradient; //visualisation of force

    private TrailRenderer redTrial;


    public void PlayerSetup(int index) // Player setup/reset
    {      
        InputManager.Instance.InputReset();

        tr = GetComponent<Transform>();
        rb = GetComponent<Rigidbody>();
        coll = GetComponent<Collider>();
        startPos = tr.position;
        noiseMotion = GetComponentInChildren<BrownianMotion>();
        noiseMotion.enablePositionNoise = false;
        coll.enabled = true;
        playerArt = tr.Find("sphere").transform;
        playerArt.localPosition = Vector3.zero;
      
        playerTrajectory = playerArt.GetComponent<LineRenderer>();

        playerTrajectory.widthMultiplier = 0;

        redTrial = tr.Find("redTrial").GetComponent<TrailRenderer>();
        redTrial.gameObject.SetActive(true);

        lRendererMaterial = playerTrajectory.material;
        playerTrail = GetComponent<TrailRenderer>();
        playerTrail.enabled = true;
        playerTrail.Clear();

        velDir = 1;
        isInDeathArea = false;
        playerArt.GetComponent<BrownianMotion>().enabled = false;
        deathTarget = Vector3.zero;
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        rb.isKinematic = true;

        if (pauseOnDrag)
            rb.drag = 1500;
        else
            rb.drag = 0;

        isActive = true;
        isSelected = false;
        isReleased = false;
        isInTarget = false;

        playerIndex = index;

        particleMain = particlesFx.main;
        particleMain.startColor = powerGradient.Evaluate(GetPercent(0));
    }

    void FixedUpdate()
    {
        if (IsInMotion())
        {
            forceRelased = false;
        }

        if (RelasePlayerAddForce())
        {
            forceRelased = false;
            rb.drag = 0;
            rb.angularDrag = 0.05f;
            rb.isKinematic = false;
            playerArt.localPosition = Vector3.zero;
            if (overrideVelocity)
                rb.AddForce(-GetComponent<Rigidbody>().velocity, ForceMode.VelocityChange);

            int snapD = -1;
            if (snapDirection == SnapDir.away) snapD = 1;
                rb.AddForce(snapD * forceVector, forceTypeToApply);

            playerTrajectory.widthMultiplier = 0;

            HideShowParticlesFX(true);

            isReleased = true;
            GameManager.Instance.PlayerReleasedSet(playerIndex);
        }
        else
        {
            forceRelased = false;
        }

        //velocity weight update
        velocityFactor += velDir * setVelSpeed * Time.deltaTime;
        velocityFactor = Mathf.Clamp01(velocityFactor);

        rb.velocity *= velocityFactor;
        rb.angularVelocity *= velocityFactor;
        forceRelased = false;

        if (posNoiseDir != 0)
            NoiseMovementWeightUpdate();

    }

    public bool IsInMotion()
    {
        if (GetVelocityMagnitude() > minVel)
            return true;
        else
            return false;
    }

    public bool RelasePlayerAddForce()
    {
        if (GameManager.Instance.gameStateGetSet == GameManager.gameState.gamePlay && forceRelased && isActive && !isReleased)
            return true;
        else
            return false;
    }

    void HideShowParticlesFX(bool hide)
    {
        if (hide && particlesFx.isPlaying)
        {
            particlesFx.Stop();
        }
        else if (!hide && particlesFx.isStopped && GameManager.Instance.IsSelected(this.gameObject) && isActive && !isReleased)
        {
            particlesFx.Play();
        }
    }

    //noise movement for player visualisation transform
    private void NoiseMovementWeightUpdate()
    {
        if (posNoiseDir > 0 && !noiseMotion.enablePositionNoise)
            noiseMotion.enablePositionNoise = true;

        if (posNoiseDir > 0 && posNoiseWeight != 1f || posNoiseDir < 0 && posNoiseWeight > 0)
        {
            posNoiseWeight += posNoiseDir * 4f * Time.deltaTime;
            posNoiseWeight = Mathf.Clamp01(posNoiseWeight);
            noiseMotion.positionFrequency = 5 * posNoiseWeight;
        }

        if (posNoiseDir > 0 && posNoiseWeight == 1f)
        {
            posNoiseDir = 0;
            Invoke("AutoDeath", 0.75f);
        }
    }


    void Update()
    {
        if (IsInDeathArea())
        {
            rb.velocity = Vector3.Lerp(rb.velocity, Vector3.zero, Time.deltaTime * 15f);
            playerArt.GetComponent<BrownianMotion>().enabled = true;
        }

        lastFrameVelocity = rb.velocity;

        if (IsInMotion())
            return;
        else
            HideShowParticlesFX(false);

        if (pauseOnDrag)
        {
            if (InputManager.Instance.isMouseDownGet)
                rb.drag = 10;
            else
                rb.drag = 0;
        }

        if (IsInAimMode())
        {
            rb.drag = 1000f;
            rb.angularDrag = 1000f;

            Vector3 JoystickPosition = InputManager.Instance.JoyPositionGet;
            Vector3 JoystickStartPosition = InputManager.Instance.JoyStartPositionGet;

            float distance = Vector3.Distance(JoystickPosition, JoystickStartPosition);

            dragDistance = Mathf.Clamp((JoystickPosition - JoystickStartPosition).magnitude, 0, magBase);

            if (dragDistance * magMultiplier < 1) dragDistance = 0;
                forceVector = JoystickPosition - JoystickStartPosition;

            forceVector.Normalize();
            forceVector *= dragDistance * magMultiplier;

            Debug.DrawRay(tr.position, (forceVector * -distance) * 0.02f, Color.red);

            int snapD = -1;
            if (snapDirection == SnapDir.away) snapD = 1;

            tr.eulerAngles = Vector3.forward* AngleBetweenPoints(JoystickStartPosition, JoystickPosition);

           PlayerTarget(tr.position, forceVector * snapD, distance);


            PlayerTrajectory(forceVector * snapD);

        }

        if (IsActive())
        {
            if (rb.velocity.magnitude > maxSpeed)
            {
                rb.velocity = rb.velocity.normalized * maxSpeed;
            }       
        }       
    }

    private bool IsInDeathArea()
    {
        if (!isActive && isInDeathArea && posNoiseWeight > 0)
            return true;
        else
            return false;
    }

    private bool IsActive()
    {
        if (GameManager.Instance.gameStateGetSet == GameManager.gameState.gamePlay && isActive)
            return true;
        else
            return false;
    }

    private bool IsInAimMode()
    {
        if (GameManager.Instance.gameStateGetSet == GameManager.gameState.gamePlay && isDragging && isActive && !isReleased)
            return true;
        else
            return false;
    }

    private void PlayerTarget(Vector3 start, Vector3 dir, float distance)
    {
        distance = Mathf.Clamp(distance, 0, magBase);

        Vector3 vStart = start;
        Vector3 vEnd = vStart + dir.normalized * (-distance*0.1f);
        vEnd = new Vector3(vEnd.x, vEnd.y, (vEnd.x+vEnd.y)*0.1f);

        playerArt.position = Vector3.Lerp(playerArt.position, vEnd, Time.deltaTime * 8f);
        particleMain.startColor = powerGradient.Evaluate(GetPercent(distance));
    }

    private float GetPercent(float distance)
    {
        float value = 0;

        value = distance / magBase * 100f;

        return value*0.01f;
    }

    private float AngleBetweenPoints(Vector2 position1, Vector2 position2)
    {
        var fromLine = position2 - position1;
        var toLine = new Vector2(0, -1);

        var angle = Vector2.Angle(fromLine, toLine);
        var cross = Vector3.Cross(fromLine, toLine);

        if (cross.z > 0)
            angle = 360f - angle;

        return angle;
    }

    //player trajectory prediction
    private void PlayerTrajectory(Vector3 force)
    {
        Vector3 velocityVector = force;

        playerTrajectory.positionCount = ((int)(maxTime / timeResolution));

        int index = 0;
        Vector3 currentPosition = tr.position;
        RaycastHit hit;
        for (float t = 0.0f; t < maxTime; t += timeResolution)
        {
            playerTrajectory.SetPosition(index, currentPosition);
            if (Physics.Raycast(currentPosition, velocityVector, out hit, velocityVector.magnitude * timeResolution, touchMask))
            {
                playerTrajectory.positionCount = index + 2;
                playerTrajectory.SetPosition(index + 1, hit.point);
                break;
            }

            currentPosition += velocityVector * timeResolution;
            velocityVector += Physics.gravity * timeResolution;

                index++;
        }

       // Debug.Log(index);
        playerTrajectory.widthMultiplier = 1;

        float aniFactor = Mathf.PingPong(Time.time * pulseSpeed, 1.0f);
        aniFactor = Mathf.Max(minWidth, aniFactor) * maxWidth;

        playerTrajectory.startWidth = aniFactor;
        playerTrajectory.endWidth = aniFactor;

        lRendererMaterial.mainTextureOffset += new Vector2(Time.deltaTime * aniDir * scrollSpeed, lRendererMaterial.mainTextureOffset.y);
    }

    private void ShatterActivator(Collision _coll)
    {
        SoundManager.Instance.PlaySoundFx(SOUND.playerShatter, 1f);
        coll.enabled = false;
        tr.gameObject.SetActive(false);
        GameObject obj = TrashMan.spawn(shatterPrefab, tr.position, tr.rotation);
        obj.GetComponent<PlayerShatter>().ActivateShatter(_coll, GetVelocity());
    }
    
    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag != "Star" && !isInTarget)
         {

            float shakePower = Mathf.Clamp(rb.velocity.magnitude * 0.025f, -1f, 1.5f);
            float relativeVel = col.relativeVelocity.magnitude;

            if (isActive && !collIsDisabled)
            {
                StartCoroutine("EnableCollision");
                if (relativeVel > maxRelativeVelPower)
                {
                    if (GameManager.Instance.playerEnergyGet < 1)
                        ShatterActivator(col);
                    else
                    {
                        //float pitch = Mathf.Clamp(relativeVel*0.01f, 0, 1.25f);

                        float pitch = Random.Range(0.85f, 1.1f);
                        SoundManager.Instance.PlayBounceSoundFx(SOUND.defaultBounce, pitch);
                    }

                    GameManager.Instance.CameraShake(shakePower);
                    SpawnManager.Instance.PostHitSpawn(tr.position);
                    GameManager.Instance.PlayerEnergyUpdate(false);
                }
            }

            if (col.gameObject.tag == "AddForceBounces")
            {
                Bounce(col.contacts[0].normal);
            }

        }


    }

    private void Bounce(Vector3 collisionNormal)
    {
        var speed = lastFrameVelocity.magnitude;
        var direction = Vector3.Reflect(lastFrameVelocity.normalized, collisionNormal);
        rb.velocity = direction * 50f;// * Mathf.Max(speed, minVel);
    }

    IEnumerator EnableCollision()
    {
        collIsDisabled = true;
        yield return new WaitForFixedUpdate();
        yield return new WaitForFixedUpdate();
        collIsDisabled = false;
    }

    private float GetVelocityMagnitude()
    {
        return rb.velocity.magnitude;
    }



    public Vector3 GetVelocity()
    {
        return rb.velocity;
    }

    public void IsInDeatrhArea(Vector3 target)
    {
        isActive = false;
        coll.enabled = false;
        isInDeathArea = true;
        velDir = -1;
        posNoiseDir = 1;
        deathTarget = target;
    }

    public void AutoDeath()
    {
        coll.enabled = false;
        tr.gameObject.SetActive(false);
        GameObject obj = TrashMan.spawn(shatterPrefab, tr.position, tr.rotation);
        obj.GetComponent<PlayerShatter>().AutoDeathShatter(tr.position, Vector3.one * 10f);

        OutOfBoundres();
        //Invoke("OutOfBoundaries", 1f);
    }

    public void UnSelectPlayer()
    {
        particlesFx.Stop();
        isSelected = false;
    }

    public void SelectPlayer()
    {
        if (!isReleased)
        {
            isSelected = true;
            particlesFx.Play();
            InputManager.Instance.SelectedPlayerGetSet = GetComponent<PlayerController>();
        }
    }

    public void TailClean()
    {
        playerTrail.Clear();
    }

    public void PlayerOnTarget()
    {
        SoundManager.Instance.PlaySoundFx(SOUND.levelEnd, 1f);
        redTrial.Clear();
        redTrial.gameObject.SetActive(false);
        playerTrail.Clear();
        playerTrail.enabled = false;
        isActive = false;
        // rb.drag = 1000;
        coll.enabled = false;
        particlesFx.Stop();
    }

    public void OutOfBoundres()
    {
        // Debug.Log("OutOfBoundaries");
        GameManager.Instance.OutOfBoundaries();
    }

    public void PlayerPause()
    {
        rb.isKinematic = true;
    }

    public void GetStar()
    {
        GameManager.Instance.LevelStarsUpdate();
    }
}
