﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MaterialUtil;


public class ShatterObjsDeactivator : MonoBehaviour {

    Rigidbody rb;
    Destructible destCtrl;
    private float actualAlpha = 0;
    private Material material;
    private bool isActive;

    void Awake ()
    {
        rb = GetComponent<Rigidbody>();
        GameObject rootObj = transform.parent.parent.gameObject;
        destCtrl = rootObj.GetComponent<Destructible>();

        if (!material)
        {
            material = GetComponent<Renderer>().material;
        }
    }

    public void Reset()
    {
        actualAlpha = 1;
        StandardShaderUtils.ChangeRenderMode(material, StandardShaderUtils.BlendMode.Opaque);
        material.color = new Color(material.color.r, material.color.g, material.color.b, 1f);
    }


    public void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Player")
        {
            destCtrl.Collision(col);
        }
    }

    public void AlphaUpdate()
    {
        if (actualAlpha > 0)
        {
            StandardShaderUtils.ChangeRenderMode(material, StandardShaderUtils.BlendMode.Fade);
            actualAlpha += -1 * 0.5f * Time.deltaTime;
            actualAlpha = Mathf.Clamp01(actualAlpha);
            Color color = new Color(material.color.r, material.color.g, material.color.b, actualAlpha);
            material.color = color;
        }
        else
            isActive = false;
    }

    private void Update()
    {
        if(!rb.isKinematic && actualAlpha> 0)
            AlphaUpdate();
    }

}
