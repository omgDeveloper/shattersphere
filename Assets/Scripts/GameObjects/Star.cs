﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31.ZestKit;

public class Star : ObjectBase
{
   // Transform tr;
   // public GameManager gameScr;

    float speed = 30f;
    float time = 0;
    float delay = 1f;

    public override void SpecialSetup()
    {
      //  GameObject levelMgrObj = GameObject.FindGameObjectWithTag("LevelController");

        GameManager.Instance.levelStarsGetSet++;

        gameObject.SetActive(true);
        ShowAnimation();

        delay = Random.value+Random.value;
        time = 0;
        isActive = true;
    }

    public override void Reset()
    {
        tr.position = spawnPosition;
        tr.eulerAngles = spawnRotation;

        gameObject.SetActive(true);
        ShowAnimation();
    }

    private void ShowAnimation()
    {
        tr.localScale = Vector3.zero;

            tr.ZKlocalScaleTo(Vector3.one, 0.5f)
                .setEaseType(EaseType.ElasticInOut)
                .setDelay(Mathf.Clamp(Random.value, 0, 0.25f))
                .start();
    }

    private void Update()
    {
        if (tr && time > delay)
            tr.Rotate(Vector3.up, Time.deltaTime * speed, Space.World);
        else
            time += Time.deltaTime;
    }


    private void LoopScaleOscylate()
    {
        tr.localScale = Vector3.one;
        if (gameObject.activeSelf)
        {

            tr.ZKlocalScaleTo(Vector3.one * 1.25f, 0.15f)
                .setLoops(LoopType.PingPong)
                .setEaseType(EaseType.ElasticInOut)
                .setDelay(Mathf.Clamp(Random.value, 0, 0.5f))
                .setCompletionHandler(tw => LoopScaleOscylate())
                .start();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        other.gameObject.GetComponent<PlayerController>().GetStar();

        this.gameObject.SetActive(false);
    }
}
