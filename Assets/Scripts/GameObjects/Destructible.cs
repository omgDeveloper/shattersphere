﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : ObjectBase
{

    private GameObject shatterRoot;
    Rigidbody[] shatterRb;
    Vector3[] shatterPos;
    Vector3[] shatterRot;
    public float minimumForce = 2.5f;
    private int playerLayer = ~(1 << 8);


    public override void Setup()
    {
        base.Setup();
        shatterRoot = transform.Find("Shatter").gameObject;
        shatterRb = shatterRoot.GetComponentsInChildren<Rigidbody>();
        shatterPos = new Vector3[shatterRb.Length];
        shatterRot = new Vector3[shatterRb.Length];

        for (int i = 0; i < shatterRb.Length; i++)
        {
            shatterPos[i] = shatterRb[i].transform.localPosition;
            shatterRot[i] = shatterRb[i].transform.localEulerAngles;
            shatterRb[i].gameObject.SetActive(true);
            shatterRb[i].gameObject.layer = 10;
        }

       // shatterRoot.SetActive(true);
        artMesh.gameObject.SetActive(false);
    }

    public override void Reset()
    {
        base.Reset();

        for (int i = 0; i < shatterRb.Length; i++)
        {
            shatterRb[i].transform.SetParent(shatterRoot.transform, false);
            shatterRb[i].transform.localPosition = shatterPos[i];
            shatterRb[i].transform.localEulerAngles = shatterRot[i];
            shatterRb[i].gameObject.SetActive(true);
            shatterRb[i].isKinematic = true;
            shatterRb[i].velocity = Vector3.zero;
            shatterRb[i].angularVelocity = Vector3.zero;
            shatterRb[i].gameObject.layer = 10;
            shatterRb[i].gameObject.GetComponent<ShatterObjsDeactivator>().Reset();
        }
     //   shatterRoot.SetActive(true);
        artMesh.gameObject.SetActive(false);
    }

    public void Collision(Collision col)
    {
        Vector3 playerVel = col.gameObject.GetComponent<PlayerController>().GetVelocity();
        if (playerVel.magnitude > minimumForce)
        {
            Collider[] colliders = Physics.OverlapSphere(col.contacts[0].point, 1f, playerLayer);
           // Debug.Log(colliders.Length);
           // shatterRoot.SetActive(true);
            for (int i = 0; i < colliders.Length; i++)
            {
                Rigidbody rb = colliders[i].GetComponent<Rigidbody>();
               // Debug.Log(rb.gameObject.name);
                rb.isKinematic = false;
                rb.AddExplosionForce(playerVel.magnitude*0.5f, col.contacts[0].point, 2, 1f, ForceMode.Impulse);
                rb.gameObject.layer = 15;
                rb.transform.SetParent(shatterRoot.transform.parent.parent, true);
            }
        }
    }


    //public override void OnCollisionEnter(Collision col)
    //{
    //    if (col.gameObject.tag == "Player")
    //    {
    //        Vector3 playerVel = col.gameObject.GetComponent<PlayerController>().GetVelocity();
    //        if (playerVel.magnitude > minimumForce)
    //        {
    //            Collider[] colliders = Physics.OverlapSphere(col.contacts[0].point, 1f);
    //            shatterRoot.SetActive(true);
    //            for (int i = 0; i < colliders.Length; i++)
    //            {
    //                shatterRb[i].isKinematic = false;
    //                shatterRb[i].AddExplosionForce(playerVel.magnitude, col.contacts[0].point, 2, 1f, ForceMode.Impulse);
    //                shatterRb[i].gameObject.layer = 15;
    //            }
    //        }
    //    }
    //}

    IEnumerator TemporaryDisabled()
    {
        for (int i = 0; i < shatterRb.Length; i++)
        {
            shatterRb[i].GetComponent<Collider>().enabled = false;
        }
            yield return new WaitForSeconds(0.2f);

        for (int i = 0; i < shatterRb.Length; i++)
        {
            shatterRb[i].GetComponent<Collider>().enabled = true;
        }
    }

}
