﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31.ZestKit;

public class PathMovement : MonoBehaviour {

    private Transform tr;

    public Vector3[] path;
    public float speed = 5f;
    private float reachDist = 0.5f;
    private int currentPoint = 0;

    public bool useZesKit = false;
    public float duration = 5f;
    public EaseType loopType = EaseType.Linear;
    public LoopType easingType = LoopType.PingPong;
    public float posDelay = 0;
    public bool useScale = false;
    public float scale = 1f;
    public float scaleDuration = 5f;
    public EaseType scaleEasing = EaseType.Linear;

    public void Awake()
    {
        tr = GetComponent<Transform>();
    }

    private void Start()
    {
        ZesAnimationInitialisation();
    }

    public void ZesAnimationInitialisation()
    {
        if (useZesKit)
        {
            var spline = new Spline(path);
            var splineTween = new SplineTween(tr, spline, duration);
            splineTween.setDelay(posDelay)
            .setEaseType(loopType)
            .setLoops(easingType, 9999)
            .start();
        }

        if(useScale)
        {
            Vector3 scaleCache = tr.localScale;

            tr.ZKlocalScaleTo(scaleCache * scale, scaleDuration).setDelay(posDelay).setLoops(easingType, 9999).setEaseType(scaleEasing).start();
        }
 


    }



    //private void Update()
    //{
    //    if (!useZesKit)
    //    {
    //        if (path != null && path.Length > 0)
    //        {
    //            float dist = Vector3.Distance(path[currentPoint], tr.localPosition);
    //            tr.localPosition = Vector3.Lerp(tr.localPosition, path[currentPoint], Time.deltaTime * speed);

    //            if (dist <= reachDist)
    //                currentPoint++;

    //            if (currentPoint >= path.Length)
    //                currentPoint = 0;
    //        }
    //    }
    //}





    private void OnDrawGizmos()
    {
        if(path.Length>0)
            for (int i = 0; i < path.Length; i++)
            {
                if(path[i] !=null)
                {
                    Gizmos.color = Color.green;
                    Gizmos.DrawSphere(path[i], reachDist);
                }
            }
    }

}
