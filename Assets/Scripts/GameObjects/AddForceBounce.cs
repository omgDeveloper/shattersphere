﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31.ZestKit;

public class AddForceBounce : MonoBehaviour
{
    float force = 5f;
    private Vector3 lastFrameVelocity;

    private Transform playerTransform;
    private float bias = 0.5f;
    Transform tr;
    Vector3 scaleCache;

    private void Awake()
    {
        tr = GetComponent<Transform>();
        scaleCache = tr.localScale;
    }

    public void OnCollisionEnter(Collision col)
    {
        if(col.collider.tag == "Player")
        {
            tr.ZKlocalScaleTo(Vector3.one * 1.35f, 0.1f).setEaseType(EaseType.BounceInOut).setLoops(LoopType.PingPong).start();
        }
    }


}
