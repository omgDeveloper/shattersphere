﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : ObjectBase
{
    private Rigidbody playerRb;

    public override void SpecialSetup()
    {
        isActive = true;
        ElastinScaleAppearance();
    }


    public override void Reset()
    {
        base.Reset();
        isActive = true;
        ElastinScaleAppearance();
    }

    public override void OnTriggerEnter(Collider other)
    {
        if (isActive && other.gameObject.tag == "Player" && GameManager.Instance.playerEnergyGet >= 0)
        {
            playerRb = other.GetComponent<Rigidbody>();
            playerRb.velocity *= 0.35f;
           // playerRb.AddForce(tr.up * 5f, ForceMode.VelocityChange);
        }
    }


    public void OnTriggerStay(Collider other)
    {
        if (isActive && other.gameObject.tag == "Player" && GameManager.Instance.playerEnergyGet >= 0)
        {
            playerRb = other.GetComponent<Rigidbody>();
            playerRb.AddForce(tr.up* 1f, ForceMode.VelocityChange);
        }
    }


}
