﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeTriggers : MonoBehaviour {

    public int index;
    public Pipe pipe;
    public BoxCollider rootColider;


    public void OnTriggerEnter(Collider other)
    {
        rootColider.enabled = false;
        pipe.SetCollision(other.gameObject, index);
    }
}
