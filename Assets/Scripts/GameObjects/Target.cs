﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31.ZestKit;

public class Target : ObjectBase
{
    public bool haveShield;
    private GameObject shieldObj;

    private Rigidbody playerRb;
    private PlayerController player;
    private ParticleSystem effect;
    private bool isInEffect;
    private float radius;
    [SerializeField] private Transform orbiterTr;

    public override void SpecialSetup()
    {
        tr = GetComponent<Transform>();
        GameObject levelMgrObj = GameObject.FindGameObjectWithTag("LevelController");
        effect = GetComponentInChildren<ParticleSystem>();
        playerRb = null;
        player = null;
        if (haveShield)
        {
            shieldObj = transform.Find("Shield").gameObject;
            StartCoroutine("ShieldActivateDelay");
        }
        transform.localScale = Vector3.zero;
        ElastinScaleAppearance();

        GetComponent<SphereCollider>().enabled = true;
        isActive = true;
        effect.Play();
        radius = GetComponent<SphereCollider>().radius;
    }

    //private void Awake()
    //{
    //    transform.localScale = Vector3.zero;
    //}

    

    public override void Reset()
    {
        base.Reset();

        GetComponent<SphereCollider>().enabled = true;
        playerRb = null;
        player = null;
        isActive = true;
        if (haveShield)
        {
            shieldObj = transform.Find("Shield").gameObject;
            StartCoroutine("ShieldActivateDelay");
        }
        transform.localScale = Vector3.zero;
        ElastinScaleAppearance();
    }


    IEnumerator ShieldActivateDelay()
    {
        yield return new WaitForSeconds(1);
        shieldObj.SetActive(true);
    }

    void MoveNest()
    {
        float dist = Vector3.Distance(tr.position, playerRb.transform.position);
        if (dist< radius && isActive)
        {
            //var party = new TweenParty(1f);
            //party.addTween(playerRb.transform.ZKpositionTo(tr.position));
            //party.addTween(playerRb.transform.ZKlocalScaleTo(Vector3.zero));
            //party.addTween(playerRb.transform.ZKlocalEulersTo(new Vector3(0, 0, 0)).setIsRelative());
            //party.setEaseType(EaseType.ExpoInOut);
            //party.setCompletionHandler(tw => ScaleTarget())
            //.start();

            //player.PlayerOnTarget();
            isActive = false;

            if (effect.isPlaying)
                effect.Stop();

        }
        else
        {
            player.setVelocityDir = 1;
            player = null;
            playerRb = null;
            isActive = true;
            effect.Play();
        }
    }

    private void Update()
    {
        if(isInEffect && isActive)
        {
            if (GameManager.Instance.IsReloadingGet)
                return;

            float dist = Vector3.Distance(tr.position, playerRb.transform.position);
            if (dist < radius*3f)
            {
                player.setVelocityDir = -1;
                playerRb.AddForce((tr.position - playerRb.transform.position), ForceMode.VelocityChange);

                if (dist < 0.5f)
                {
                    if (effect.isPlaying)
                        effect.Stop();
                    var party = new TweenParty(0.75f);
                    party.addTween(playerRb.transform.ZKpositionTo(tr.position));
                    party.addTween(playerRb.transform.ZKlocalScaleTo(Vector3.zero));
                    party.setEaseType(EaseType.ExpoOut);
                    party.setCompletionHandler(tw => ScaleDisapperance())
                    .start();
                    isActive = false;
                    isInEffect = false;
                    player.PlayerOnTarget();
                }
            }
            else
            {
                isInEffect = false;
                if (player != null)
                {
                    player.setVelocityDir = 1;
                    player.IsInTargetGetSet = false;
                    player = null;
                    playerRb = null;
                    effect.Play();
                }
            }
        }

        orbiterTr.Rotate(Vector3.up * 15* Time.deltaTime);
    }
  

    public virtual void ZeroStars()
    {
        shieldObj.SetActive(false);
    }

    public override  void OnTriggerEnter(Collider other)
    {
        if (isActive && other.gameObject.tag == "Player" && GameManager.Instance.playerEnergyGet>= 0)
        {
            player = other.GetComponent<PlayerController>();

            playerRb = other.GetComponent<Rigidbody>();
            isInEffect = true;
            player.IsInTargetGetSet = true;
           // Invoke("MoveNest", 0.5f);
        }
    }



}
