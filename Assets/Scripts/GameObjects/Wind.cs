﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wind : ObjectBase
{

    List<Rigidbody> windAffectdObjs;

    public float forcePower = 10f;
    public float radious = 4f;
    public float maxAangleRange = 45f;
    public ParticleSystem windParticleFx;

    public override void SpecialSetup()
    {

        ParticleSystem.ShapeModule shape = windParticleFx.shape;
        shape.angle = maxAangleRange-10f;
        windParticleFx.startSpeed = radious * 2f;

        isActive = true;
    }



    void Update()
    {
        if (isActive)
        {
            Collider[] colls;
            colls = Physics.OverlapSphere(tr.position + tr.up * radious, radious);
            windAffectdObjs = new List<Rigidbody>();

            foreach (Collider coll in colls)
            {
                Rigidbody rb = coll.GetComponent<Rigidbody>();
                if (rb)
                    windAffectdObjs.Add(rb);
            }

            foreach (Rigidbody rbs in windAffectdObjs)
            {
                Vector3 dir = tr.position - rbs.transform.position;
                float angleRange = AngleBetweenVectors3(tr.position, rbs.transform.position);

                if (angleRange < maxAangleRange)
                {
                    float distance = Vector3.Distance(tr.position, rbs.position);
                    // float percent = distance / radious * 100f;
                    rbs.AddForce((-dir* forcePower) / distance, ForceMode.Force);
                    // Debug.Log("force:"+dir/percent+"percent:"+percent);
                }
            }

        }
    }

    private float AngleBetweenVectors3(Vector3 vec1, Vector3 vec2)
    {
        Vector3 diference = vec2 - vec1;
        float sign = (vec2.y < vec1.y) ? -1.0f : 1.0f;
        return Vector3.Angle(tr.up, diference) * sign;
    }

    void OnDrawGizmos()
    {
        Transform tr_ = GetComponent<Transform>();
        Gizmos.DrawWireSphere(tr_.position + tr_.up * radious, radious);
    }


}
