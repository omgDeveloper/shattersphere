﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31.ZestKit;

public class ObjectBase : MonoBehaviour {

    [HideInInspector] public Transform tr;
    [HideInInspector] public Vector3 spawnPosition;
    [HideInInspector] public Vector3 spawnRotation;
    [HideInInspector] public Transform artMesh;
    [HideInInspector] public float angle;

    public bool isActive;





    public virtual void Setup()
    {
        tr = GetComponent<Transform>();
        spawnPosition = tr.position;
        spawnRotation = tr.eulerAngles;
        angle = tr.eulerAngles.z;

        artMesh = tr.Find("ArtMesh").transform;
        SpecialSetup();
    }

    public virtual void SpecialSetup()
    {
        isActive = true;
    }

    public virtual void Reset()
    {
        tr.position = spawnPosition;
        tr.eulerAngles = spawnRotation;
        angle = tr.eulerAngles.z;

        isActive = true;
    }


    public virtual void OnCollisionEnter(Collision col)
    {
    
    }
    public virtual void OnTriggerEnter(Collider other)
    {

    }

    public virtual void OnTriggerExit(Collider other)
    {

    }

    public virtual void TouchObjectAction()
    {
        Debug.Log(tr.gameObject.name+ " I'm touched");
    }

    public void ElastinScaleAppearance()
    {
        float random = Random.value;
        tr.localScale = Vector3.zero;
        tr.ZKlocalScaleTo(Vector3.one, 0.5f)
                    .setEaseType(EaseType.ElasticInOut)
                    .setDelay(0.1f + random)
                    .start();
    }

    public void ScaleDisapperance()
    {
        
        tr.ZKlocalScaleTo(Vector3.zero, 0.75f)
                    .setCompletionHandler(tw => GameManager.Instance.LevelComplete())
                    .setEaseType(EaseType.BackInOut)
                    .setDelay(0.25f)
                    .start();
    }

}
