﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31.ZestKit;

public class RotateMotion : MonoBehaviour
{

    private Transform tr;

    public float speed = 5f;

    public void Awake()
    {
        tr = GetComponent<Transform>();
    }

    private void  Update()
    {

            tr.Rotate(Vector3.forward * speed * Time.deltaTime, Space.World);
        
    }





}
