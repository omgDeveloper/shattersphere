﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31.ZestKit;

public class Pipe : ObjectBase
{

    SphereCollider[] triggers;
    GameObject obj;
    int triggerIndex;
    public float forcePower = 10f;
    public float rotateAngle = 45f;

    TweenParty party;
    TweenParty party1;

    bool isRotate;

    public override void SpecialSetup()
    {
        StopAllCoroutines();
        triggers = GetComponentsInChildren<SphereCollider>();
        isActive = true;
    }

    public override void Reset()
    {
        StopAllCoroutines();
        tr.position = spawnPosition;
        tr.eulerAngles = spawnRotation;
        angle = tr.eulerAngles.z;

        obj = null;
        triggerIndex = -1;

        foreach (SphereCollider pipeTriggers in triggers)
            pipeTriggers.enabled = true;

        GetComponent<BoxCollider>().enabled = true;
    }


    public void SetCollision(GameObject _obj, int index)
    {
        obj = _obj;
        triggerIndex = index;

        GetObj();
    }

    void GetObj()
    {
        StartCoroutine("DisableTriggers");

        if(party !=null) party.stop();

        party = new TweenParty(0.25f);
        party.addTween(obj.transform.ZKlocalScaleTo(Vector3.one*0.1f));
        party.addTween(obj.transform.ZKpositionTo(tr.position));
        party.setEaseType(EaseType.CircInOut).setDelay(0.2f).setCompletionHandler(t => ShotObj());
        party.start();

        var chain = new TweenChain();
        chain.appendTween(artMesh.ZKlocalScaleTo(Vector3.one * 1.25f, 0.25f).setEaseType(EaseType.ExpoOut).setDelay(0.2f));
        chain.appendTween(artMesh.ZKlocalScaleTo(Vector3.one, 0.25f).setEaseType(EaseType.ExpoIn));
        chain.start();


    }

    void ShotObj()
    {
        obj.transform.rotation = triggers[triggerIndex].transform.rotation;

        if(party1 != null) party1.stop();

        party1 = new TweenParty(0.25f);
        party1.addTween(obj.transform.ZKlocalScaleTo(Vector3.one));
        party1.addTween(obj.transform.ZKpositionTo(triggers[triggerIndex].transform.position));
        party1.setEaseType(EaseType.ElasticIn);


            party1.start();

        StartCoroutine("EnableTriggers");
    }

    IEnumerator EnableTriggers()
    {
        Rigidbody objRb = obj.GetComponent<Rigidbody>();
        obj.gameObject.GetComponent<PlayerController>().TailClean();

            objRb.drag = 0;
            objRb.velocity = Vector3.zero;
            objRb.angularVelocity = Vector3.zero;

            Vector3 forceDir = obj.transform.up;
            objRb.AddForce(forceDir * forcePower, ForceMode.Impulse);
            yield return new WaitForSeconds(0.5f);

       // GetComponentInChildren<MeshCollider>().enabled = true;

        foreach (SphereCollider pipeTriggers in triggers)
            pipeTriggers.enabled = true;

        GetComponent<BoxCollider>().enabled = true;
        yield return null;
    }

    IEnumerator DisableTriggers()
    {
        Rigidbody objRb = obj.GetComponent<Rigidbody>();
        obj.gameObject.GetComponent<PlayerController>().TailClean();

        objRb.drag = 10f;

     //   GetComponentInChildren<MeshCollider>().enabled = false;

        foreach (SphereCollider pipeTriggers in triggers)
            pipeTriggers.enabled = false;

        yield return null;
    }

    void RotatePipe()
    {
        isRotate = true;
        TweenParty rotateParty = new TweenParty(0.5f);
        rotateParty.addTween(tr.ZKlocalEulersTo(new Vector3(tr.localEulerAngles.x, tr.localEulerAngles.y, tr.localEulerAngles.z+ rotateAngle)));
        rotateParty.setEaseType(EaseType.ElasticInOut).setCompletionHandler(t => isRotate = false);

        rotateParty.start();
    }


    public override void TouchObjectAction()
    {
        RotatePipe();
    }

}
