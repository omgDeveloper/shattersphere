﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//shatter element

public class PlayerShatterRbs : MonoBehaviour {

    private float actualAlpha = 0;
    private Material material;
    private bool isActive;

    public void Setup()
    {
            material = GetComponent<Renderer>().material;

        actualAlpha = 1;

        var color = material.color;
        color.a = 1f;


        material.SetColor("_BaseColor", color);
        isActive = true;
    }

    //shatter element disaperance
    public void AlphaUpdate()
    {
        if (actualAlpha > 0)
        {
            actualAlpha += -1 * 0.25f * Time.deltaTime;
            actualAlpha = Mathf.Clamp01(actualAlpha);
            var color = material.color;
            color.a = actualAlpha;
            material.SetColor("_BaseColor", color);

        }
        else
            isActive = false;

    }

    private void Update()
    {
        if (isActive)
            AlphaUpdate();
        else
        {
            TrashMan.despawn(this.gameObject);
        }
    }
}
