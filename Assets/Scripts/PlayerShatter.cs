﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//fake player shatter controller

public class PlayerShatter : MonoBehaviour {

    Rigidbody[] shatterRb;
    Vector3[] shatterPos;
    Vector3[] shatterRot;
    PlayerShatterRbs[] shatterScr;
    int length;


    private void Awake()
    {
        length = GetComponentsInChildren<Rigidbody>().Length;
        shatterPos = new Vector3[length];
        shatterRot = new Vector3[length];
        shatterScr = new PlayerShatterRbs[length];
        shatterRb = GetComponentsInChildren<Rigidbody>();

        for (int i = 0; i < length; i++)
        {
            shatterPos[i] = shatterRb[i].transform.localPosition;
            shatterRot[i] = shatterRb[i].transform.localEulerAngles;
            shatterScr[i] = shatterRb[i].GetComponent<PlayerShatterRbs>();
        }
    }

    //reset shatter element for reuse
    public void Reset()
    {

        for (int i = 0; i < length; i++)
        {
            shatterRb[i].transform.localPosition = shatterPos[i];
            shatterRb[i].transform.localEulerAngles = shatterRot[i];
            shatterRb[i].velocity = Vector3.zero;
            shatterRb[i].angularVelocity = Vector3.zero;
        }
    }

    public void ActivateShatter(Collision col, Vector3 velocity)
    {

        for (int i = 0; i < length; i++)
        {
            shatterScr[i].Setup();
        }

        for (int i = 0; i < length; i++)
        {
            shatterRb[i].AddExplosionForce(velocity.magnitude * 0.5f, col.contacts[0].point, 2, 1f, ForceMode.Impulse);
        }

    }

    public void AutoDeathShatter(Vector3 pos, Vector3 velocity)
    {

        for (int i = 0; i < length; i++)
        {
            shatterScr[i].Setup();
        }

        for (int i = 0; i < length; i++)
        {
            shatterRb[i].AddExplosionForce(velocity.magnitude * 0.5f, pos, 2, 1f, ForceMode.Impulse);
        }

    }



}
