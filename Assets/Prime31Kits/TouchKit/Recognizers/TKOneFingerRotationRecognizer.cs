using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;



/// <summary>
/// detects a rotation around an object with a single finger. The target objects position must be provided in screen coordinates.
/// </summary>
public class TKOneFingerRotationRecognizer : TKRotationRecognizer
{
	public new event Action<TKOneFingerRotationRecognizer> gestureRecognizedEvent;
	public new event Action<TKOneFingerRotationRecognizer> gestureCompleteEvent;
	
	/// <summary>
	/// this should be the center point in screen coordinates of the object that is being rotated
	/// </summary>
	public Vector2 targetPosition;

   // private Vector2 _startPoint;
    private Vector2 _endPoint;
    public int minimumNumberOfTouches = 1;
    public int maximumNumberOfTouches = 2;
    public float _angle = 0;
  
    public float angle
    {
        get
        {
            return this._angle;
        }
    }
    /*
       public Vector2 startPoint
       {
           get
           {
               return this._startPoint;
           }
       }
   */
    public Vector2 endPoint
    {
        get
        {
            return this._endPoint;
        }
    }

    internal override void fireRecognizedEvent()
	{
		if( gestureRecognizedEvent != null )
			gestureRecognizedEvent( this );
	}
	
	
	internal override bool touchesBegan( List<TKTouch> touches )
	{
		if( state == TKGestureRecognizerState.Possible )
		{
            /*
            for (int i = 0; i < touches.Count; i++)
            {
                // only add touches in the Began phase
                if (touches[i].phase == TouchPhase.Began)
                {
                    _trackingTouches.Add(touches[i]);
                    _startPoint = touches[0].position;

                    if (_trackingTouches.Count == maximumNumberOfTouches)
                        break;
                }
            }
            */
                _trackingTouches.Add( touches[0] );
			
			deltaRotation = 0;
			_previousRotation = angleBetweenPoints( targetPosition, _trackingTouches[0].position );
			state = TKGestureRecognizerState.Began;
		}
		
		return false;
	}
	
	
	internal override void touchesMoved( List<TKTouch> touches )
	{
		if( state == TKGestureRecognizerState.RecognizedAndStillRecognizing || state == TKGestureRecognizerState.Began )
		{
			 _angle = angleBetweenPoints( targetPosition, _trackingTouches[0].position );
			deltaRotation = Mathf.DeltaAngle(_angle, _previousRotation );
			_previousRotation = _angle;
			state = TKGestureRecognizerState.RecognizedAndStillRecognizing;
		}
	}
	
	
	internal override void touchesEnded( List<TKTouch> touches )
	{
        _endPoint = touchLocation();
        // if we had previously been recognizing fire our complete event
        if ( state == TKGestureRecognizerState.RecognizedAndStillRecognizing )
		{
			if( gestureCompleteEvent != null )
				gestureCompleteEvent( this );
		}
		
		state = TKGestureRecognizerState.FailedOrEnded;
	}

}
